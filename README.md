# Litter Pick 0.1.0

A web app for tracking litter you pick

[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~33+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [unreleased]

[unreleased]: https://gitlab.com/bytesnz/litter-pick/compare/master...mvp

