#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const data = {};

if (process.argv.length !== 4) {
  console.error('usage: mkReadme <source> <output>');
  process.exit(1);
}

const getDeep = (object, pathParts) => {
  if (pathParts && pathParts.length) {
    const key = pathParts.shift();

    if (object instanceof Object) {
      return getDeep(object[key], pathParts);
    } else {
      return;
    }
  }

  return object;
}

const source = process.argv[2];
const dir = path.dirname(source);
const out = process.argv[3];

// Replace <!=include <file>>
let readme = fs.readFileSync(source).toString();
readme = readme.replace(/<!=include ([^>]+)>/g, (match, includePath) => {
  const resolved = path.resolve(includePath);

  try {
    fs.accessSync(resolved, fs.constants.R_OK);

    return fs.readFileSync(resolved).toString();
  } catch (error) {
    console.log(error);
    console.error(`Error reading file '${includePath}' in '${dir}': ${error.message}`);
    return '';
  }
});

// Replace <!=<json_file (without .json)> <path/to/value>>
readme = readme.replace(/<!=(.+) ([^>]+)>/g, (match, file, key) => {
  if (data[file] === false) {
    return '';
  } else if (!data[file]) {
    const resolved = path.resolve(`${file}.json`);
    try {
      data[file] = require(resolved);
    } catch (error) {
      console.error(`Error reading file '${file} in ${dir}': ${error.message}`);
      data[file] = false;
      return '';
    }
  }

  if (key.indexOf('/') !== -1) {
    return getDeep(data[file], key.split('/')) || '';
  } else {
    return data[file][key] || '';
  }
});

fs.writeFileSync(out, readme);
console.log(`${out} written`);
