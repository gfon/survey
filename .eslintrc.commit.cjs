module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  rules: {
    '@typescript-eslint/no-inferrable-types': 0,
    '@typescript-eslint/no-use-before-define': 1,
    '@typescript-eslint/no-unused-vars': [2, {
      args: 'none',
      varsIgnorePattern: 'h'
    }],
    // TODO Disabled as prettier can't handle export default as Tool<>
    '@typescript-eslint/consistent-type-assertions': 0,
    'no-console': [2, {allow: ['warn', 'error']}],
    'no-debugger': 2,
    'no-warning-comments': [2, { terms: ['xxx'], location: 'anywhere' }],
    'require-jsdoc': 2
  },
  overrides: [
    {
      files: ['utils/*.ts', 'utils/*.js'],
      extends: ['plugin:node/recommended'],
      rules: {
        '@typescript-eslint/no-var-requires': 0
      }
    },
    {
      files: ['src/server/*.ts'],
      rules: {
        'no-console': [2, {allow: ['warn', 'error', 'log']}]
      }
    }
  ]
};
