import { render, h } from 'preact';
import Router from 'preact-router';
import { IntlProvider } from 'preact-i18n';

import Header from './components/header';
import App from './App';
import About from './pages/about';
import { Store } from './store/index';
import IconSet from './components/iconSet';

import Welcome from './components/welcome';
import AddLitter from './components/add-litter';
import LitterList from './components/litter-list';
import LitterSurvey from './components/litter-survey';

const definition = {};

render(
  <Store>
    <IntlProvider>
      <IconSet/>
      <Header/>
      <main>
        <Router>
          <AddLitter path="/add" default />
          <LitterList path="/list" />
          <LitterSurvey path="/survey" />
        </Router>
      </main>
    </IntlProvider>
  </Store>,
  document.querySelector('#root')
);
//      <About path="/about" />
//      <ServiceWorkder path="/about/serviceworker" />
//      <Data path="/about/data" />
