interface ActionsObject<T> {
  [key: string]: (state: T, ...args: any) => T | Promise<T>;
}

export type Actions<T> = ActionsObject<T> | ((store) => ActionsObject<T>);
