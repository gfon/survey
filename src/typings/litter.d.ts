import { Coordinate, Bounds } from './location';

export interface Survey {
  id: string;
  start: Date;
  end?: Date;
  bounds?: Bounds;
  track?: Array<Coordinate>;
}

export interface Litter {
  position: Coordinate;
  time: Date;
  photos?: Array<{
    id: string;
    blob?: Blob;
    url?: string;
  }>;
}
