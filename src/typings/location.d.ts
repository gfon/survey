
/**
 * Location in [ latitude, longitude ]
 */
export type Location = [
  /// Latitude
  number,
  /// Longitude
  number
];

/**
 * Bounds of a location
 */
export type Bounds = [
  Location,
  Location
];
