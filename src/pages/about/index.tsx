import { h } from 'preact';
import { Texti, IntlProvider } from 'preact-i18n';
import SiteDetails from '../../context/siteDetails';

const About = () => {
  const siteDetails = useContext(SiteDetails);

  return (
    <IntlProvider scope="about">
      <h1>
        <Text id="main.title" props={{ name: siteDetails.title }}>
          About { siteDetails.title }
        </Text>
      </h1>
    </IntlProvider>
  );
};

export default About;

