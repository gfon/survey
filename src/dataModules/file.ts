import { getRelativePath } from '../lib/path';

export default (name: string, privateClient, publicClient) => ({
  /**
   * Store the file. File will be stored publically unless the
   * file has a truey private property
   *
   * @param file File to store
   *
   * @returns A Promise that resolves with the stored file once it
   *   has been stored
   */
  store: (id: string, file, isPublic: boolean = true, relativeTo?: string) => {
    const client = isPublic ? publicClient : privateClient;

    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.addEventListener('loadend', () => {
        resolve(reader.result);
      });
      reader.readAsArrayBuffer(file);
    })
      .then((image) => client.storeFile(file.type, `${name}/${id}`, image))
      .then(() => {
        if (client.storage.remote.connected) {
          return client.getItemURL(`${name}/${id}`);
        } else if (relativeTo) {
          return getRelativePath(`${name}/${id}`, relativeTo);
        } else {
          return `${name}/${id}`;
        }
      });
  },
  /**
   * Removes the file with the given id
   *
   * @param id ID of file to remove
   * @param isPublic Whether the file is currently private
   *
   * @returns A Promise once the file has been removed
   */
  remove: (id: string, isPublic: boolean = true) => {
    const client = isPublic ? publicClient : privateClient;
    return client.remove(`${name}/${id}`);
  },
  /**
   * Toggle whether an file is public or not
   *
   * @param id ID of file to remove
   * @param isPublic Whether the file is currently private
   *
   * @returns A Promise once the file's publicity has been changed
   */
  changePublic: (id: string, isPublic: boolean = true) => {
    const storeClient = isPublic ? privateClient : publicClient;
    const getClient = isPublic ? publicClient : privateClient;
    return getClient
      .getFile(`${name}/${id}`)
      .then((file) =>
        Promise.all([
          getClient.remove(`${name}/${id}`),
          storeClient.storeFile(file.mimeType, `${name}/${id}`, file.data)
        ]).then(() => file)
      );
  },
  /**
   * Get an file
   *
   * @param id ID of file to remove
   * @param isPublic Whether the file is currently private
   *
   * @returns A Promise that resolves to the file
   */
  get: (id: string, isPublic: boolean = true) => {
    const client = isPublic ? publicClient : privateClient;
    return client.getFile(`${name}/${id}`).then((file) => {
      if (file && file.data) {
        const blob = new Blob([file.data], { type: file.contentType });
        blob.revision = file.revision;
        return blob;
      }

      return;
    });
  },
  /**
   * Get a list of all files
   *
   * @param isPublic If given will only get either all public files
   *   (true), or all private file (false). Otherwise all public and
   *   private files will be retrieved
   *
   * @returns A Promise that resolves to the files
   */
  listAll: (isPublic: boolean = true) => {
    const promises = [];
    if (isPublic !== true) {
      // get private
      promises.push(privateClient.getListing(`${name}/`));
    }
    if (isPublic !== false) {
      // get public
      promises.push(publicClient.getListing(`${name}/`));
    }

    return Promise.all(promises).then((results) => {
      if (results.length === 2) {
        Object.assign(results[0], results[1]);
      }

      return results[0];
    });
  },
  /**
   * Get all files
   *
   * @param isPublic If given will only get either all public files
   *   (true), or all private file (false). Otherwise all public and
   *   private files will be retrieved
   *
   * @returns A Promise that resolves to the files
   */
  getAll: (isPublic: boolean = true) => {
    const promises = [];
    if (isPublic !== true) {
      // get private
      promises.push(privateClient.getAll(`${name}/`));
    }
    if (isPublic !== false) {
      // get public
      promises.push(publicClient.getAll(`${name}/`));
    }

    return Promise.all(promises).then((results) => {
      if (results.length === 2) {
        Object.assign(results[0], results[1]);
      }

      return results[0];
    });
  }
});
