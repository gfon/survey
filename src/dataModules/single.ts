/**
 * ReemoteStorage data module builder for a single file JSON Object
 *
 * @param name Name of store. Will be used as the name for the schema and the
 *   folder name
 * @param privateClient RemoteStorage private client
 * @param publicClient RemoteStorage public client
 * @param uri URI (ID) for the object schema
 * @param schema The schema for the stored objects
 *
 * @returns A RemoteStore data module for the given JSON Object
 */
export default (name: string, privateClient, publicClient, uri, schema) => {
  if (!schema) {
    schema = {
      type: 'object'
    };
  } else if (!uri) {
    uri = schema['$id'];
  }

  if (uri) {
    publicClient.declareType(name, uri, schema);
    privateClient.declareType(name, uri, schema);
  } else {
    publicClient.declareType(name, schema);
    privateClient.declareType(name, schema);
  }

  return {
    /**
     * Store the object. Object will be stored publically unless the
     * object has a truey private property
     *
     * @param object Object to store
     *
     * @returns A Promise that resolves with the stored object once it
     *   has been stored
     */
    store: (object, isPublic: boolean = true) => {
      const client = isPublic ? publicClient : privateClient;

      return client.storeObject(
        name, name, object
      ).then(() => object)
    },
    /**
     * Removes the object with the given id
     *
     * @param isPublic Whether the object is currently private
     *
     * @returns A Promise once the object has been removed
     */
    remove: (isPublic: boolean = true) => {
      const client = isPublic ? publicClient : privateClient;
      return client.remove(name);
    },
    /**
     * Toggle whether an object is public or not
     *
     * @param isPublic Whether the object is currently public
     *
     * @returns A Promise once the object's publicity has been changed
     */
    changePublic: (isPublic: boolean = true) => {
      const storeClient = isPublic ? privateClient : publicClient;
      const getClient = isPublic ? publicClient : privateClient;
      return getClient.getObject(name).then((object) => {
        return Promise.all([
          getClient.remove(name),
          storeClient.storeObject(name, name, object)
        ]).then(() => object);
      });
    },
    /**
     * Get an object
     *
     * @param isPublic Whether the object is currently private
     *
     * @returns A Promise that resolves to the object
     */
    get: (isPublic: boolean = true) => {
      const client = isPublic ? publicClient : privateClient;
      return client.getObject(name);
    }
  };
}
