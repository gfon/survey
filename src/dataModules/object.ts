import { makeId } from '../lib/id';

/**
 * RemoteStorage data module builder for a JSON Object store
 *
 * @param name Name of store. Will be used as the name for the schema and the
 *   folder name
 * @param privateClient RemoteStorage private client
 * @param publicClient RemoteStorage public client
 * @param uri URI (ID) for the object schema
 * @param schema The schema for the stored objects
 *
 * @returns A RemoteStore data module for the given JSON Object store
 */
export default (name: string, privateClient, publicClient, uri, schema) => {
  if (!schema) {
    schema = {
      type: 'object'
    };
  } else if (!uri) {
    uri = schema['$id'];
  }

  if (uri) {
    publicClient.declareType(name, uri, schema);
    privateClient.declareType(name, uri, schema);
  } else {
    publicClient.declareType(name, schema);
    privateClient.declareType(name, schema);
  }

  return {
    /**
     * Store the object. Object will be stored publically unless the
     * object has a truey private property
     *
     * @param object Object to store
     *
     * @returns A Promise that resolves with the stored object once it
     *   has been stored
     */
    store: (object) => {
      const client = object.private ? privateClient : publicClient;
      let id = null;
      if (typeof object['@id'] !== 'string') {
        id = makeId()
        object = {
          ...object,
          '@id': client.getItemURL(`${name}/${id}`) || id
        };
      } else {
        id = object['@id'].slice(object['@id'].lastIndexOf('/') + 1);
      }

      return client.storeObject(
        name, `${name}/${id}`, object
      ).then(() => object)
    },
    /**
     * Removes the object with the given id
     *
     * @param id ID of object to remove
     * @param isPublic Whether the object is currently private
     *
     * @returns A Promise once the object has been removed
     */
    remove: (id: string, isPublic: boolean = true) => {
      const client = isPublic ? publicClient : privateClient;
      return client.remove(`${name}/${id}`);
    },
    /**
     * Toggle whether an object is public or not
     *
     * @param id ID of object to remove
     * @param isPublic Whether the object is currently public
     *
     * @returns A Promise once the object's publicity has been changed
     */
    changePublic: (id: string, isPublic: boolean = true) => {
      const storeClient = isPublic ? privateClient : publicClient;
      const getClient = isPublic ? publicClient : privateClient;
      return getClient.getObject(`${name}/${id}`).then((object) => {
        if (isPublic) {
          delete object.private;
        } else {
          object.private = true;
        }

        return Promise.all([
          getClient.remove(`${name}/${id}`),
          storeClient.storeObject(name, `${name}/${id}`, object)
        ]).then(() => object);
      });
    },
    /**
     * Get an object
     *
     * @param id ID of object to remove
     * @param isPublic Whether the object is currently private
     *
     * @returns A Promise that resolves to the object
     */
    get: (id: string, isPublic: boolean = true) => {
      const client = isPublic ? publicClient : privateClient;
      return client.getObject(`${name}/${id}`);
    },
    /**
     * Get a list of all objects
     *
     * @param isPublic If given will only get either all public objects
     *   (true), or all private object (false). Otherwise all public and
     *   private objects will be retrieved
     *
     * @returns A Promise that resolves to the objects
     */
    listAll: (isPublic: boolean = true) => {
      let promises = [];
      if (isPublic !== true) { // get private
        promises.push(privateClient.getListing(`${name}/`));
      }
      if (isPublic !== false) { // get public
        promises.push(publicClient.getListing(`${name}/`));
      }

      return Promise.all(promises).then((results) => {
        if (results.length === 2) {
          Object.assign(results[0], results[1]);
        }

        return results[0];
      });
    },
    /**
     * Get all objects
     *
     * @param isPublic If given will only get either all public objects
     *   (true), or all private object (false). Otherwise all public and
     *   private objects will be retrieved
     *
     * @returns A Promise that resolves to the objects
     */
    getAll: (isPublic: boolean = true) => {
      let promises = [];
      if (isPublic !== true) { // get private
        promises.push(privateClient.getAll(`${name}/`));
      }
      if (isPublic !== false) { // get public
        promises.push(publicClient.getAll(`${name}/`));
      }

      return Promise.all(promises).then((results) => {
        if (results.length === 2) {
          Object.assign(results[0], results[1]);
        }

        return results[0];
      });
    }
  };
}
