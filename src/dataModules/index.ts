import fileBuilder from './file';
import objectBuilder from './object';
import singleObjectBuilder from './single';

type ModuleObject = {
  type: 'file';
} | {
  type: 'object';
  uri?: string;
  schema?: string;
}

/**
 * Create a remoteStorage object data module for the given name and schema
 *
 * @param name Name (root directory) of data module
 * @param objects Objects that are part of the module
 *
 * @returns A remoteStorage data module that can then be passed to
 *   in the modules array when initialising remoteStorage
 */
export const createObjectModule = (
  name: string,
  objects: { [name: string]: ModuleObject }
) => ({
  name,
  builder: (privateClient, publicClient) => {
    const builtModule = {};
    const names = Object.keys(objects);
    for (let i = 0; i < names.length; i++) {
      switch (objects[names[i]].type) {
        case 'file':
          builtModule[names[i]] = fileBuilder(
            names[i], privateClient, publicClient
          );
          break;
        case 'object':
          builtModule[names[i]] = objectBuilder(
            names[i], privateClient, publicClient,
            objects[names[i]].uri, objects[names[i]].schema
          );
          break;
        case 'singleObject':
          builtModule[names[i]] = singleObjectBuilder(
            names[i], privateClient, publicClient,
            objects[names[i]].uri, objects[names[i]].schema
          );
          break;
      }
    }

    return {
      exports: builtModule
    };
  }
});

export default (name: string) => createObjectModule(name, {
  images: {
    type: 'file'
  },
  surveys: {
    type: 'object',
    uri: 'https://schema.gfon.org/surveys/v0/Survey'
  },
  litter: {
    type: 'object',
    uri: 'https://schema.gfon.org/litter/v0/LitterObservation'
  },
  nonSurveyLitter: {
    type:'singleObject',
    uri: 'https://schema.gdon.org/litter/v0/NonSurveyLitter'
  }
});
