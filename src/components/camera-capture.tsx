import { h, Component, createRef } from 'preact';

interface VideoState {
  videoCapture?: boolean;
  mounted: boolean;
  clientWidth?: number;
}

interface VideoProps {
  onError?: (error: Error) => void;
  onNewPhoto?: (photo: Blob) => void;
  enabled?: boolean;
}

class CameraCapture extends Component<VideoProps, VideoState> {
  state = {
    videoCapture: null,
    mounted: false,
    clientWidth: null
  };
  stream: MediaStream = null;
  video = createRef();
  canvas = createRef();

  constructor(props) {
    super();

    this.setStream(props);
  }

  setStream(props) {
    const { onError } = props || this.props;
    if (
      navigator &&
      navigator.mediaDevices &&
      navigator.mediaDevices.getUserMedia
    ) {
      navigator.mediaDevices
        .getUserMedia({
          audio: false,
          video: {
            facingMode: 'environment'
          }
        })
        .then((stream) => {
          this.setState({ videoCapture: true });
          this.stream = stream;
        })
        .catch((error) => {
          if (onError) {
            onError(error);
          }
          this.setState({ videoCapture: false });
        });
    } else {
      this.setState({ videoCapture: false });
    }
  }

  stopVideo() {
    if (this.video.current && this.video.current.srcObject) {
      this.video.current.pause();
      this.video.current.srcObject = null;
      this.video.current.src = '';
      this.video.current.load();
      this.video.current.removeAttribute('src');
    }
    if (this.stream) {
      const tracks = this.stream.getTracks();
      for (let i = 0; i < tracks.length; i++) {
        tracks[i].stop();
      }
      this.stream = null;
      this.setState({ videoCapture: null });
    }
  }

  takePhoto() {
    return new Promise((resolve, reject) => {
      if (
        this.canvas &&
        this.canvas.current &&
        this.video &&
        this.video.current
      ) {
        const canvas = this.canvas.current;
        const video = this.video.current;
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
          this.video.current,
          0,
          0,
          video.videoWidth,
          video.videoHeight
        );
        canvas.toBlob((blob) => {
          if (this.props.onNewPhoto) {
            this.props.onNewPhoto(blob);
          }
          resolve(blob);
        });
      } else {
        reject(new Error('No canvas or video'));
      }
    });
  }

  connectStream() {
    if (this.video && this.video.current) {
      this.video.current.srcObject = this.stream;
      // Get the resolution of the stream
      const tracks = this.stream.getVideoTracks();

      if (tracks.length) {
        this.video.current.play();
      }
    }
  }

  componentDidMount() {
    this.setState({
      mounted: true
    });
  }

  componentDidUpdate() {
    if (this.props.enabled === false) {
      this.stopVideo();
    } else {
      if (this.state.videoCapture === null) {
        this.setStream(this.props);
        return;
      }
      if (
        this.video.current &&
        !this.video.current.srcObject &&
        this.state.videoCapture &&
        this.state.mounted &&
        this.stream
      ) {
        this.connectStream();
      }
    }
  }

  componentWillUnmount() {
    this.stopVideo();
  }

  render() {
    return (
      <div class="capture">
        {this.state.videoCapture === null ? null : this.state.videoCapture ? (
          <div>
            <canvas ref={this.canvas} />
            <video ref={this.video} />
          </div>
        ) : null}
      </div>
    );
  }
}

export default CameraCapture;
