import { h } from 'preact';
import { useContext } from 'preact/hooks';
import { IntlProvider, Text, MarkupText } from 'preact-i18n';
import { Link } from 'preact-router';

import SiteDetails from '../context/siteDetails';

const Welcome = ({ onAgree }) => {
  const siteDetails = useContext(SiteDetails);
  const agree = onAgree || (() => {});

  return (
    <IntlProvider scope="welcome">
      <section class="welcome">
        <h1>
          <Text id="title" props={{ title: siteDetails.title }}>
            Welcome to {siteDetails.title}
          </Text>
        </h1>
        <p>
          <Text id="thankYou">
            Thank you for wanting to record litter you see and clean up.
            Recording it will help figure out the main types of litter and where
            it is most prevalent.
          </Text>
        </p>
        <h2>
          <Text id="what.title">The Data</Text>
        </h2>
        <p>
          <Text id="what.intro">We would love if you could:</Text>
        </p>
        <ul>
          <li>
            <Text id="what.photos">
              take a photos of the litter before you pick it up and if it has a
              label, the front if it, so that it can be recognised.
            </Text>
          </li>
          <li>
            <Text id="what.type">
              record the type of object it is, such as a bottle or a straw
            </Text>
          </li>
          <li>
            <Text id="what.material">
              record main material it is made from, such as plastic or metal
            </Text>
          </li>
          <li>
            <Text id="what.brand">record the brand and product name</Text>
          </li>
        </ul>
        <h2>
          <Text id="permissions.title">App Permissions</Text>
        </h2>
        <p>
          <Text id="permissions.intro">
            To help you record this information, this app can use the following:
          </Text>
        </p>
        <dl>
          <dt>
            <Text id="permissions.camera.name">the camera</Text>
          </dt>
          <dd>
            <Text id="permissions.camera.description">
              so that you can take photos of the litter
            </Text>
          </dd>
          <dt>
            <Text id="permissions.location.name">your location</Text>
          </dt>
          <dd>
            <Text id="permissions.location.description">
              so we can record where the litter was
            </Text>
          </dd>
          <dt>
            <Text id="permissions.storage.name">persistent storage</Text>
          </dt>
          <dd>
            <Text id="permissions.storage.description">
              so this app can be used offline and store the data you collect
              safely. Data will be saved automatically when you connect back to
              the internet.
            </Text>
          </dd>
        </dl>
        <p>
          <Text id="permissions.asking">
            Your browser will ask for permission for this app to use your
            location and camera. It will also ask for permission to use
            persistent storage.
          </Text>
        </p>
        <h2>
          <Text id="offline.title">Offline Use</Text>
        </h2>
        <p>
          <MarkupText id="offline.description">
            This app can be configured to work offline using a Service Worker.
            Please choose if you want to us to use a Service Worker to ensure we
            get your litter when you are offline. You can change your decision
            later by clicking on the database at the top of the screen.
          </MarkupText>
        </p>
        <details>
          <summary>
            <Text id="offline.serviceworker.whatis.title">
              What is a service worker?
            </Text>
          </summary>

          <MarkupText id="offline.serviceworker.whatis.description">
            A service worker is script that runs in the background in your
            browser. It is used to cache files, such as images and other scripts
            for the web site, and can synchronise data without the user needing
            to be in the web site. Many web sites use them, but don't ask for
            permission.
          </MarkupText>
        </details>
        <button onClick={() => agree(true)}>
          <Text id="agree">
            Yes, configure for offline use and background sync
          </Text>
        </button>
        <button onClick={() => agree(false)}>
          <Text id="disagree">
            No, I will only use this when I have an internet connection
          </Text>
        </button>
      </section>
    </IntlProvider>
  );
};

export default Welcome;
