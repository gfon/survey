import { h } from 'preact';
import { useRef } from 'preact/hooks';
import { FromStore } from '../store';

import { IntlProvider } from 'preact-i18n';

const LitterSurvey = ({
  picks,
  picksSurveySet,
  picksLitterClear,
  picksSurveyUpdate,
  statusSetStatus
}) => {
  const track = useRef(null);

  const startPick = async () => {
    if (track.current.value) {
      statusSetStatus('trackPosition', true);
    }
    await picksSurveySet({
      startTime: new Date(),
      endTime: null
    });
    await picksLitterClear();
  };

  const finishPick = async () => {
    await picksSurveyUpdate({
      endTime: new Date()
    });
    await picksSurveySet(null);
    picksLitterClear();
  };

  return (
    <IntlProvider scope="litterSurvey">
      {picks.survey ? (
        <section class="litterSurvey">
          <h1>Survey underway</h1>
          <p>
            Started:
            <time dateTime={picks.survey.startTime.toISOString()}>
              {picks.survey.startTime.toLocaleString()}
            </time>
          </p>
          <button onClick={finishPick}>Finish</button>
        </section>
      ) : (
        <section class="litterSurvey">
          <h1>Start a new survey</h1>
          <label class="checkbox">
            <input ref={track} type="checkbox" checked />
            Track location during the survey
          </label>
          <button onClick={startPick}>Start</button>
        </section>
      )}
    </IntlProvider>
  );
};

export default FromStore('picks')(LitterSurvey);
