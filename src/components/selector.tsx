import { h, createRef } from 'preact';
import { useMemo, useState, useEffect } from 'preact/hooks';
import { makeMap } from '../lib/utils';

export interface Item {
  id: string;
  title: string;
  label?: string;
  items?: Array<Item>;
}

interface ScoredItem extends Item {
  _score?: number;
}

const sortItems = (items: Array<ScoredItem>, priorityItems: Array<string>) => {
  let score = null;

  items = items.slice();

  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    if (item.items) {
      const result = sortItems(item.items, priorityItems);
      item.items = result.items;
      item._score = result.score;
    } else {
      item._score = priorityItems.indexOf(item.id);
    }
    if (item._score !== -1) {
      score = score === null ? item._score : Math.min(item._score, score);
    }
  }

  items.sort((a, b) => {
    if (b._score === -1 && a._score === -1) {
      return 0;
    }

    if (b._score === -1 || a._score < b._score) {
      return -1;
    }

    return 1;
  });

  return { score: score === null ? -1 : score, items };
};

/**
 * Filter list of items based on given search term
 *
 * @param items Items to filter on
 * @param search Search term
 *
 * @returns A list of filtered items
 */
const filterItems = (items, search) => {
  const filtered = [];
  if (!(search instanceof RegExp)) {
    search = new RegExp(search, 'i');
  }

  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    if (item.items) {
      const filteredItems = filterItems(item.items, search);
      item = {
        ...item,
        items: filteredItems.length ? filteredItems : null
      };
    }

    if (
      item.items ||
      search.exec(item.title) ||
      (item.label && search.exec(item.label))
    ) {
      filtered.push(item);
    } else if (item.otherNames?.length) {
      for (let j = 0; j < item.otherNames.length; j++) {
        if (search.exec(item.otherNames[j])) {
          filtered.push(item);
        }
      }
    }
  }

  return filtered;
};

export default ({
  label,
  items,
  priorityItems,
  value,
  onInput,
  onExpanded,
  expanded,
  keepExpanded,
  recent,
  defaultRecentExpanded,
  allowCustom,
  itemsMap
}) => {
  const defaultItemsExpanded = () => (defaultRecentExpanded ? ['_recent'] : []);

  const [isExpanded, setExpanded] = useState(expanded || false);
  const [expandedItems, setExpandedItems] = useState(defaultItemsExpanded());
  const [inputValue, setInputValue] = useState(null);
  const container = createRef();

  useEffect(() => {
    if (expanded !== isExpanded) {
      setExpanded(expanded);
    }
  }, [expanded]);

  useEffect(() => {
    if (onExpanded) {
      onExpanded(isExpanded);
    }
    if (isExpanded === 1) {
      container.current?.scrollIntoView();
    }
  }, [isExpanded]);

  useEffect(() => {
    if (!value) {
      setInputValue(null);
    } else {
      if (!selectedItem && inputValue !== value) {
        setInputValue(value);
      }
    }
  }, [value]);

  const memoItemsMap = useMemo(() => {
    if (itemsMap) {
      return itemsMap;
    }

    return makeMap(items);
  }, [items, itemsMap]);

  const recentItems = useMemo(() => {
    if (recent?.length && memoItemsMap) {
      const items = [];

      for (let i = 0; i < recent.length; i++) {
        if (memoItemsMap[recent[i]]) {
          items.push(memoItemsMap[recent[i]]);
        }
      }

      return items;
    }

    return null;
  }, [recent, memoItemsMap]);

  const sortedItems = useMemo(() => {
    if (!priorityItems) {
      return items;
    }

    return sortItems(items, priorityItems).items;
  }, [items, priorityItems]);

  const selectedItem = value ? memoItemsMap[value] : null;

  let filteredItems = null;

  if (inputValue && (!selectedItem || inputValue !== selectedItem.title)) {
    filteredItems = filterItems(sortedItems, inputValue);
  }

  const onInputChange = (event) => {
    setInputValue(event.target.value);
    if (allowCustom && onInput) {
      onInput({
        value: event.target.value
      });
    }
  };

  const onInputFocus = (event) => {
    if (isExpanded) {
      container.current?.scrollIntoView();
      return;
    }

    setExpanded(1);
    event.target.blur();
  };

  const clickDiv = (event, item) => {
    if (event.defaultPrevented || event.target.nodeName !== 'DIV') {
      return;
    }

    event.preventDefault();

    if (item.items) {
      toggle(item.id);
    } else if (item.selectable !== false) {
      select(item);
    }
  };

  const toggle = (id) => {
    const index = expandedItems.indexOf(id);
    if (index === -1) {
      setExpandedItems([...expandedItems, id]);
    } else {
      const newExpanded = expandedItems.slice();
      newExpanded.splice(index, 1);
      setExpandedItems(newExpanded);
    }
  };

  const select = (item) => {
    setInputValue(null);
    setExpanded(keepExpanded || false);
    setExpandedItems(defaultItemsExpanded());
    if (onInput) {
      onInput({
        value: item.id,
        item
      });
    }
  };

  const makeItems = (currentItems) =>
    currentItems.map((item) => {
      const itemExpanded =
        filteredItems || expandedItems.indexOf(item.id) !== -1;
      return (
        <div
          class={`items${item === selectedItem ? ' selected' : ''}`}
          onClick={(event) => clickDiv(event, item)}
        >
          {item.selectable !== false ? (
            <button
              class="item"
              aria-selected={item === selectedItem || null}
              onClick={() => select(item)}
            >
              {item.label || item.title}
            </button>
          ) : (
            item.label || item.title
          )}
          {item.items ? (
            <button
              aria-expanded={itemExpanded || null}
              onClick={() => toggle(item.id)}
            >
              {itemExpanded ? 'Collapse' : 'Expand'}
            </button>
          ) : null}
          {item.items && itemExpanded ? makeItems(item.items) : null}
        </div>
      );
    });

  let showItems;
  if (isExpanded) {
    if (!inputValue && recentItems) {
      showItems = [
        {
          id: '_recent',
          title: 'Recent',
          items: recentItems
        },
        ...(filteredItems || sortedItems)
      ];
    } else {
      showItems = filteredItems || sortedItems;
    }
  }

  return (
    <div class="selector" ref={container}>
      <div class="flex">
        <label class={inputValue || selectedItem ? ' value' : null}>
          <span>{label}</span>
          <input
            value={inputValue || (selectedItem && selectedItem.title) || ''}
            onInput={onInputChange}
            onFocus={onInputFocus}
          />
        </label>
        <button
          aria-expanded={isExpanded || null}
          onClick={() => setExpanded(isExpanded ? false : 1)}
        >
          {isExpanded ? 'Collapse' : 'Expand'}
        </button>
      </div>
      {isExpanded ? makeItems(showItems) : null}
    </div>
  );
};
