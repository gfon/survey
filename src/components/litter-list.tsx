import { h, Fragment } from 'preact';
import { useState, useEffect, useMemo } from 'preact/hooks';
import { FromStore } from '../store';
import { getLitterLocation, makeLitterHierarchy } from '../lib/litter';
import { useRefState } from '../lib/hooks';

import { IntlProvider, Text } from 'preact-i18n';
import Map from '../components/map';
import Hierarchy from './hierarchy';
import Icon from './icon';
import ObservationForm from './observationForm';

const Litter = ({
  picks,
  picksImagesLoad,
  picksGetHistory,
  picksGetSurveyLitter,
  picksLitterUpdate
}) => {
  const [clickedLitter, setClickedLitter, clickedLitterRef] = useRefState(null);
  const [editedLitter, setEditedLitter, editedLitterRef] = useRefState(null);
  const [highlightedLitter, setHighlighedLitter] = useState(null);
  const [currentExpanded, setExpanded] = useState(false);
  const [surveyExpanded, setSurveyExpanded] = useState([]);
  const [currentHierarchy, setCurrent] = useState(null);
  const [surveyHierarchy, setSurveyHierarchy] = useState({});
  const [hasLocation, setHasLocation] = useState(false);

  const edited = useMemo(() => {
    if (clickedLitter === editedLitter) {
      return false;
    }
    // TODO Test actual values
    return true;
  }, [clickedLitter, editedLitter]);

  // Create current litter hierarchy
  useEffect(() => {
    // Group in to types
    if (picks.litter.length) {
      setCurrent(makeLitterHierarchy(picks.litter));
    } else {
      setCurrent(null);
    }
  }, [picks.litter]);

  // Set has location if the survey or litter has locations
  useEffect(() => {
    if (picks.litter?.length) {
      for (let i = 0; i < picks.litter.length; i++) {
        if (getLitterLocation(picks.litter[i])) {
          if (!hasLocation) {
            setHasLocation(true);
          }
          return;
        }
      }
    }
    if (picks.history?.surveys?.length && surveyExpanded?.length) {
      for (let i = 0; i < picks.history.surveys.length; i++) {
        const survey = picks.history.surveys[i];
        if (surveyExpanded.indexOf(survey['@id']) === -1) {
          continue;
        }
        if (survey.location?.length) {
          if (!hasLocation) {
            setHasLocation(true);
          }
          return;
        }
        const surveyLitter = picks.history.litter?.[surveyExpanded[i]];
        if (surveyLitter) {
          for (let j = 0; j < surveyLitter.length; j++) {
            if (getLitterLocation(surveyLitter[i])) {
              if (!hasLocation) {
                setHasLocation(true);
              }
              return;
            }
          }
        }
      }
    }
    if (hasLocation) {
      setHasLocation(false);
    }
  }, [surveyExpanded, picks.history?.surveys, picks.litter]);

  // Create previous survey litter hierarchies
  useEffect(() => {
    if (picks?.history?.litter) {
      const keys = Object.keys(picks?.history?.litter);

      if (keys.length) {
        const newHierarchy = {};

        for (let i = 0; i < keys.length; i++) {
          if (surveyHierarchy[keys[i]]) {
            // TODO Check up-to-date
            newHierarchy[keys[i]] = surveyHierarchy[keys[i]];
          } else {
            newHierarchy[keys[i]] = makeLitterHierarchy(
              picks.history.litter[keys[i]]
            );
          }
        }

        setSurveyHierarchy(newHierarchy);
      }
    }
  }, [picks?.history?.litter]);

  // Ensure images are loaded
  useEffect(() => {
    if (!picks.imagesLoaded) {
      picksImagesLoad();
    }
  }, [picks.imagesLoaded]);

  // Get litter history
  useEffect(() => {
    if (!picks.history) {
      picksGetHistory();
    }
  }, [picks.history]);

  const updateEditedLitter = (litter) => {
    if (editedLitter) {
      setEditedLitter({
        ...editedLitter,
        ...litter
      });
    }
  };

  const saveEditedLitter = () => {
    picksLitterUpdate(editedLitter).then(() => {
      editedLitterRef.set(null);
      clickedLitterRef.set(null);
    });
  };

  const clickLitter = (litter) => {
    if (clickedLitter === litter) {
      setClickedLitter(null);
      setEditedLitter(null);
    } else {
      setClickedLitter(litter);
      setEditedLitter(litter);
    }
  };

  const toggleCurrentExpanded = () => {
    setExpanded(!currentExpanded);
  };

  const toggleSurveyExpanded = (id) => {
    const index = surveyExpanded.indexOf(id);

    if (index === -1) {
      setSurveyExpanded([...surveyExpanded, id]);

      if (!picks.history?.litter?.[id]) {
        picksGetSurveyLitter(id);
      }
    } else {
      const newExpanded = surveyExpanded.slice();
      newExpanded.splice(index, 1);
      setSurveyExpanded(newExpanded);
    }
  };

  const highlightLitter = (litter) => {
    if (highlightedLitter !== litter) {
      setHighlighedLitter(litter);
    }
  };

  const clearHightlight = (litter) => {
    if (highlightedLitter === litter) {
      setHighlighedLitter(null);
    }
  };

  const hierarchyElement = (items = [], expanded) => (
    <Hierarchy
      items={items}
      selectable={false}
      expanded={expanded}
      itemHeader={({ item, expandButton }) => (
        <div class="flex">
          <div class="grow">
            {item.item.title}
            {expandButton}
          </div>
          <div>{item.count}</div>
        </div>
      )}
    >
      {({ item }) => (
        <div class="images">
          {item.litter?.map((litter) => (
            <button
              onClick={() => clickLitter(litter)}
              onMouseover={() => highlightLitter(litter)}
              onMouseout={() => clearHightlight(litter)}
            >
              {litter.media?.length ? (
                <img src={litter.media[0].url} />
              ) : (
                <Icon double icon="camera-off" />
              )}
            </button>
          ))}
        </div>
      )}
    </Hierarchy>
  );

  return (
    <IntlProvider scope="litterList">
      <section class="litterList">
        {hasLocation ? (
          <Map
            class="map"
            markedPositionBehaviour={clickedLitter ? 'pan' : 'zoom'}
            markedPosition={getLitterLocation(
              highlightedLitter
                ? highlightedLitter.position
                : clickedLitter?.position
            )}
          />
        ) : null}
        <div class="sliderContainer">
          {!picks.litter.length ? (
            'No litter picked yet'
          ) : (
            <Fragment>
              <div class="flex header">
                <h1>
                  {picks.survey ? (
                    <Text id="currentSurveyListTitle">Current Survey</Text>
                  ) : (
                    <Text id="currentSession">Current Session</Text>
                  )}
                </h1>
                <button onClick={toggleCurrentExpanded}>
                  {currentExpanded ? 'Collapse all' : 'Expand All'}
                </button>
              </div>
              {hierarchyElement(currentHierarchy || [], currentExpanded)}
            </Fragment>
          )}
          {picks.history?.surveysCount ? (
            <Fragment>
              <h1>Other Surveys</h1>
              {picks.history.surveys.map((item) => {
                if (item['@id'] === picks.survey?.['@id']) {
                  return null;
                }
                const expanded = surveyExpanded.indexOf(item['@id']) !== -1;

                return (
                  <Fragment>
                    <div class="flex header">
                      <div class="grow">
                        <time dateTime="item.startTime">
                          {item.startTime.toLocaleString()}
                        </time>
                        <button
                          onClick={() => toggleSurveyExpanded(item['@id'])}
                        >
                          {expanded ? 'Hide' : 'Show'}
                        </button>
                      </div>
                      <div>{item.observations?.length || 0} item(s)</div>
                    </div>
                    {expanded && surveyHierarchy[item['@id']]
                      ? hierarchyElement(surveyHierarchy[item['@id']], null)
                      : null}
                  </Fragment>
                );
              })}
            </Fragment>
          ) : null}
          {picks.history?.otherLitterCount ? (
            <Fragment>
              <div class="flex header">
                <div class="flex grow">
                  <h1>Other Litter</h1>
                  <button onClick={() => toggleSurveyExpanded(null)}>
                    {surveyExpanded.indexOf(null) !== -1 ? 'Hide' : 'Show'}
                  </button>
                </div>
                <div>{picks.history?.otherLitterCount}</div>
              </div>
              {surveyExpanded.indexOf(null) !== -1 && surveyHierarchy[null]
                ? hierarchyElement(surveyHierarchy[null], null)
                : null}
            </Fragment>
          ) : null}
          <div
            class={'flex col slider' + (clickedLitter ? ' open' : '')}
            style="right: 0; padding: 0 1rem;"
          >
            <div style="text-align: right">
              <button
                class="icon"
                style="font-size: 1.5em"
                onClick={() => setClickedLitter(null)}
              >
                &times;
              </button>
            </div>
            {clickedLitter ? (
              <Fragment>
                <div style="margin-bottom: 1rem;">
                  <ObservationForm
                    expandMedia={true}
                    alwaysShowMedia={true}
                    observation={editedLitter}
                    startMainExpanded={false}
                    onInput={updateEditedLitter}
                  />
                </div>
                <div style="position: sticky; bottom: 0">
                  <button disabled={!edited} onClick={saveEditedLitter}>
                    Save
                  </button>
                </div>
              </Fragment>
            ) : null}
          </div>
        </div>
      </section>
    </IntlProvider>
  );
};

export default FromStore('picks')(Litter);
