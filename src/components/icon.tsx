import { h } from 'preact';

export const Icon = (props) => {
  let { size } = props;
  const { icon, double, label, color } = props;
  if (!size) {
    size = double ? '2em' : '1.5em';
  }

  return (
    <svg
      class={['icon-' + icon, ...(props['class'] || [])].join(' ')}
      style={{height: size, width: size, fill: color || null}}
      aria-label={label}>
      <use xlinkHref={`#icon-${icon}`} />
    </svg>
  );
};

export const IconButton = ({ icon, double, size, label, color, ...props }) => (
  <button class={`icon${props['class'] ? ' ' + props['class'] : ''}`} {...props}>
    <Icon {...{icon, double, size, label, color}} />
  </button>
);

export default Icon;
