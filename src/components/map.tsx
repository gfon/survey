import { Coordinate } from '../typings/location';

import { h } from 'preact';
import { useRef, useLayoutEffect } from 'preact/hooks';
import { useRefState, useCssVar, usePropRef } from '../lib/hooks';
import { FromStore } from '../store';
import { getCurrentPosition, toLatLng } from '../lib/location';
import 'leaflet/dist/leaflet.css';
import { getSurveyTrack } from '../lib/survey';

let LCached = null;
let leafletPromise = null;

interface MapState {
  litterLayers: Map;
  map?: HTMLDivElement;
  currentPositionMarker?: any;
  currentPosition?: Coordinate;
  markedPositionMarker?: any;
  markedPosition?: Coordinate;
  track?: Array<any>;
  trackLayer?: any;
  litter?: null;
}

const MapEl = ({
  currentPosition,
  markedPosition,
  picks,
  settings,
  onClick,
  settingsSetPermission,
  ...props
}) => {
  const colors = useCssVar({
    litter: 'map-litter-color',
    currentLocation: 'map-current-location-color',
    markedLocation: 'map-marked-location-color'
  });
  const div = useRef(null);
  const mapStateRef = useRefState<MapState>({
    litterLayers: new Map()
  })[2];
  const runningLayoutEffect = useRefState(false)[2];
  const onClickRef = usePropRef(onClick);
  const autoPositionRef = useRefState(false)[2];
  const [L, , LRef] = useRefState(LCached);

  const handleMapClick = (event) => {
    if (onClickRef.current) {
      onClickRef.current(event);
    }
  };

  useLayoutEffect(() => {
    if (runningLayoutEffect.value) {
      return;
    }
    if (!L) {
      leafletPromise = import('leaflet').then((leaflet) => {
        LCached = leaflet.default;
        LRef.set(LCached);
        leafletPromise = null;
      });
      return;
    }
    runningLayoutEffect.set(true);
    const newState = Object.assign({}, mapStateRef.value);
    let zoom = 16;
    let change = false;
    let promise;
    if (div.current && !mapStateRef.value.map) {
      let center = [0, 0];
      const bounds = [
        [null, null],
        [null, null]
      ];

      const expand = (position) => {
        if (bounds[0][0] === null || position.latitude < bounds[0][0]) {
          bounds[0][0] = position.latitude;
        }
        if (bounds[1][0] === null || position.latitude > bounds[1][0]) {
          bounds[1][0] = position.latitude;
        }
        if (bounds[0][1] === null || position.longitude < bounds[0][1]) {
          bounds[0][1] = position.longitude;
        }
        if (bounds[1][1] === null || position.longitude > bounds[1][1]) {
          bounds[1][1] = position.longitude;
        }
      };

      if (picks.litter.length || picks.survey) {
        if (picks.litter.length) {
          for (let i = 0; i < picks.litter.length; i++) {
            if (picks.litter[i].position) {
              expand(picks.litter[i].position);
            }
          }
        }
      }

      if (currentPosition) {
        promise = Promise.resolve(currentPosition);
      } else {
        if (settings.permissions.location !== false) {
          promise = getCurrentPosition({
            enableHighAccuracy: false,
            timeout: 1000
          }).catch((error) => {
            if (error.code && error.code == 1) {
              settingsSetPermission('location', false);
            }
            return null;
          });
        } else {
          promise = Promise.resolve(null);
        }

        promise = promise
          .then((position) => {
            if (!position) {
              return fetch('/geo')
                .then((response) => response.json())
                .then(
                  (geoData) => {
                    if (geoData?.ll) {
                      zoom = 12;
                      return {
                        latitude: geoData.ll[0],
                        longitude: geoData.ll[1]
                      };
                    }

                    return null;
                  },
                  (error) => null
                );
            }

            return position;
          })
          .then((position) => {
            if (position) {
              center = [position.latitude, position.longitude];
              expand(position);
            }
          });
      }

      promise = promise.then((position) => {
        if (!div.current) {
          return;
        }
        const newMap = L.map(div.current, {
          trackResize: true,
          center,
          zoom,
          layers: [
            L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
              attribution:
                '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
            })
          ]
        });
        if (bounds[0][0] !== null) {
          newMap.fitBounds(bounds, {
            maxZoom: 17
          });
        }
        newMap.on('click', handleMapClick);
        newState.map = newMap;
        change = true;
      });
    } else {
      promise = Promise.resolve();
    }

    Promise.all([promise, leafletPromise]).then(() => {
      if (!div.current) {
        return;
      }
      if (picks.litter !== newState.litter) {
        const oldKeys = Array.from(newState.litterLayers.keys());
        const newMap = new Map(newState.litterLayers);
        newState.litterLayers = newMap;
        change = true;

        for (let i = 0; i < picks.litter.length; i++) {
          const litter = picks.litter[i];
          const index = oldKeys.indexOf(litter);

          if (index === -1) {
            newMap.set(
              litter,
              litter.location?.[0]
                ? L.circleMarker(
                    [litter.location[0].latitude, litter.location[0].longitude],
                    {
                      radius: 5,
                      stroke: false,
                      fillOpacity: 0.8,
                      fillColor: colors.current.litter
                    }
                  ).addTo(newState.map)
                : null
            );
          } else {
            oldKeys.splice(index, 1);
          }
        }

        if (oldKeys.length) {
          for (let i = 0; i < oldKeys.length; i++) {
            newMap.get(oldKeys[i]).remove();
            newMap.delete(oldKeys[i]);
          }
        }
      }

      const surveyTrack = getSurveyTrack(picks.survey);
      if (newState.track !== surveyTrack) {
        if (surveyTrack?.length > 1) {
          if (newState.trackLayer) {
            let i;
            for (i = 0; i < newState.track.length; i++) {
              if (newState.track[i] !== surveyTrack[i]) {
                break;
              }
            }
            if (i < surveyTrack.length) {
              const newLatLngs = newState.trackLayer.getLatLngs().slice(0, i);

              for (i; i < surveyTrack.length; i++) {
                newLatLngs.push(toLatLng(surveyTrack[i]));
              }

              newState.track = surveyTrack;
              change = true;
              newState.trackLayer.setLatLngs(newLatLngs);
            }
          } else {
            const latLngs = surveyTrack.map((position) => toLatLng(position));
            newState.trackLayer = L.polyline(latLngs, {
              weight: 1,
              color: colors.current.currentLocation
            }).addTo(newState.map);
            newState.track = surveyTrack;
            change = true;
          }
        } else {
          if (newState.trackLayer) {
            newState.trackLayer.remove();
            newState.trackLayer = null;
            change = true;
          }
        }
      }

      if (markedPosition !== newState.markedPosition) {
        if (markedPosition) {
          const latlon = [markedPosition.latitude, markedPosition.longitude];
          if (newState.markedPositionMarker) {
            newState.markedPositionMarker.setLatLng(latlon);
          } else {
            newState.markedPositionMarker = L.circleMarker(latlon, {
              radius: 8,
              stroke: false,
              fillColor: colors.current.markedLocation,
              fillOpacity: 1
            }).addTo(newState.map);
          }
        } else {
          if (newState.markedPositionMarker) {
            newState.markedPositionMarker.remove();
            newState.markedPositionMarker = null;
          }
        }
        newState.markedPosition = markedPosition;
        change = true;
      }

      if (currentPosition !== newState.currentPosition) {
        if (currentPosition) {
          const latlon = [currentPosition.latitude, currentPosition.longitude];
          if (newState.currentPositionMarker) {
            newState.currentPositionMarker.setLatLng(latlon);
          } else {
            newState.currentPositionMarker = L.circleMarker(latlon, {
              radius: 8,
              fill: false,
              opacity: 0.7,
              color: colors.current.currentLocation
            }).addTo(newState.map);
          }
          if (!onClickRef.current && !autoPositionRef.value) {
            newState.map.panTo(latlon);
          }
        } else {
          if (newState.currentPositionMarker) {
            newState.currentPositionMarker.remove();
            newState.currentPositionMarker = null;
          }
        }
        newState.currentPosition = currentPosition;
        change = true;
      }

      if (change) {
        mapStateRef.set(newState);
      }

      runningLayoutEffect.set(false);
    });
  }, [L, div, currentPosition, markedPosition, picks.litter]);

  useLayoutEffect(() => {
    if (mapStateRef.value.litterLayers.size) {
      mapStateRef.value.litterLayers.forEach((layer) => {
        if (layer) {
          layer.setStyle({
            fillColor: colors.current.litter
          });
        }
      });
    }

    if (mapStateRef.value.markedPositionMarker) {
      mapStateRef.value.markedPositionMarker.setStyle({
        fillColor: colors.current.markedLocation
      });
    }

    if (mapStateRef.value.currentPositionMarker) {
      mapStateRef.value.currentPositionMarker.setStyle({
        color: colors.current.currentLocation
      });
    }
  }, [colors.current]);

  return (
    <div class={props['class']}>
      <div ref={div} />
    </div>
  );
};

export default FromStore(['picks', 'settings'])(MapEl);
