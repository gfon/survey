import { h, Fragment } from 'preact';
import { IntlProvider, Text } from 'preact-i18n';
import Selector from '../components/selector';
import { useEffect, useState } from 'preact/hooks';
import { FromStore } from '../store';

import mcsObjects, { mcsUrl, makeAttributes, map } from '../taxon/mcsObjects';
import materials from '../taxon/materials';
import brands from '../taxon/brands';

const idOrString = (value) =>
  value ? (typeof value === 'string' ? value : value['@id']) : null;

const ObservedForm = ({
  observed,
  onInput,
  recent,
  startMainExpanded = true,
  advancedSelectors
}) => {
  const [mcsObject, setMcsObject] = useState(null);
  const [mcsExpanded, setMcsExpanded] = useState(startMainExpanded);
  const [materialExpanded, setMaterialExpanded] = useState(false);

  useEffect(() => {
    if (observed?.['@id']?.startsWith(mcsUrl)) {
      setMcsObject(map[observed['@id'].slice(mcsUrl.length)]);
    }
  }, [observed?.['@id']]);

  useEffect(() => {
    if (!observed) {
      setMcsExpanded(startMainExpanded);
      setMaterialExpanded(false);
      setMcsObject(null);
    }
  }, [observed]);

  const inputType = (event) => {
    if (onInput) {
      onInput({
        ...(observed || {}),
        '@id': event.value
      });
    }
  };

  const inputAttribute = (attribute, event) => {
    if (onInput) {
      onInput({
        ...(observed || {}),
        [attribute]: {
          '@id': event.value
        }
      });
    }
  };

  const inputBrand = (event) => {
    if (onInput) {
      if (event.item) {
        onInput({
          ...(observed || {}),
          brand: {
            '@id': event.value
          }
        });
      } else {
        onInput({
          ...(observed || {}),
          brand: event.value
        });
      }
    }
  };

  const inputMcsObject = (event) => {
    if (onInput) {
      onInput({
        ...(observed || {}),
        '@id': mcsUrl + event.value,
        ...(makeAttributes(event.item) || {})
      });
    }

    if (mcsExpanded) {
      setMcsExpanded(false);
    }
  };

  return (
    <IntlProvider scope="litterPick">
      {advancedSelectors ? (
        <Fragment>
          <Selector
            label="Material"
            items={materials}
            value={idOrString(observed?.material)}
            onInput={(event) => inputAttribute('material', event)}
          />
        </Fragment>
      ) : (
        <Fragment>
          <Selector
            label="Litter Type"
            items={mcsObjects}
            expanded={mcsExpanded}
            value={mcsObject && mcsObject.id}
            onInput={inputMcsObject}
            onExpanded={setMcsExpanded}
            recent={recent.mcs}
            defaultRecentExpanded={true}
          />
          {mcsObject?.id?.match(/-other/) ? (
            <label class={observed ? 'value' : null}>
              <span>
                <Text id="type">Type</Text>
              </span>
              <input value={observed?.type || ''} onInput={inputType} />
            </label>
          ) : null}
        </Fragment>
      )}
      <Selector
        label="Brand"
        items={brands}
        value={idOrString(observed?.brand)}
        expanded={materialExpanded}
        onExpanded={setMaterialExpanded}
        recent={recent.brand}
        onInput={inputBrand}
        filterProperties={observed}
        defaultRecentExpanded={true}
        allowCustom={true}
      />
    </IntlProvider>
  );
};

export default FromStore('recent')(ObservedForm);
