import { h } from 'preact';
import { Text } from 'preact-i18n';
import ObservedForm from './observedForm';
import { IconButton } from '../components/icon';
import { FromStore } from '../store';
import { usePropRef } from '../lib/hooks';
import { datetimeFormat } from '../lib/utils';

const ObservationForm = ({
  alwaysShowMedia,
  expandMedia,
  getCurrentPosition,
  grabMapClick,
  observation,
  onRequestMapPosition,
  onInput,
  settings,
  settingsSetPermission,
  startMainExpanded = false
}) => {
  const settingsSetPermissionRef = usePropRef(settingsSetPermission);
  const onInputRef = usePropRef(onInput);

  const updateObserved = (data, index) => {
    if (onInput) {
      if (!observation) {
        const observed = [];
        observed[index] = data;
        onInput({
          observed
        });
      } else {
        const observed = observation?.observed || [];
        observed[index] = data;
        onInput({
          ...observation,
          observed
        });
      }
    }
  };

  const inputPosition = (event) => {
    const match = event.target.value.match(
      /^(?<lat>-?\d+(\.\d*)?), *(?<lon>-?\d+(\.\d*)?)$/
    );

    if (match && onInput) {
      onInput({
        ...(observation || {}),
        location: [
          {
            latitude: match.groups.lat,
            longitude: match.groups.lon
          }
        ]
      });
    }
  };

  const inputTime = (event) => {
    if (onInput) {
      const time = new Date(event.target.value);

      if (!isNaN(time.getTime())) {
        onInput({
          ...(observation || {}),
          time
        });
      }
    }
  };

  const useCurrentTime = () => {
    if (onInput) {
      onInput({
        ...(observation || {}),
        time: new Date()
      });
    }
  };

  const useCurrentPosition = () => {
    getCurrentPosition().then((position) => {
      onInputRef.current({
        location: [position]
      });
      if (!settings.permissions.location) {
        settingsSetPermissionRef.current('location', true);
      }
    });
  };

  return (
    <div class="observationForm">
      {alwaysShowMedia || observation?.media?.length ? (
        <details class="images" open={expandMedia || null}>
          <summary>
            <Text
              id="photos"
              plural={observation?.media?.length || 0}
              fields={{ count: observation?.media?.length || 0 }}
            >
              Photos ({observation?.media?.length || 0} photo(s))
            </Text>
          </summary>
          {observation?.media?.map((image) => (
            <div>
              <img src={image.url} />
              <IconButton icon="delete" class="delete" />
            </div>
          )) || null}
        </details>
      ) : null}
      <div class="flex">
        <div class="flex m6">
          <label class={observation?.location?.[0] ? 'value' : null}>
            <span>
              <Text id="location">Location</Text>
            </span>
            <input
              type="tel"
              pattern="^-?\d+(\.\d*)?, *-?\d+(\.\d*)?$"
              value={
                observation?.location?.[0]
                  ? `${observation.location[0].latitude}, ${observation.location[0].longitude}`
                  : ''
              }
              onInput={inputPosition}
            />
          </label>
          {onRequestMapPosition ? (
            <IconButton
              title="Choose location on map"
              icon="map-marker"
              color={grabMapClick ? '#000' : '#ccc'}
              onClick={onRequestMapPosition}
            />
          ) : null}
          {settings.permissions.location !== false ? (
            <IconButton
              title="Use current location"
              icon="crosshairs-gps"
              onClick={useCurrentPosition}
            />
          ) : null}
        </div>
        <div class="flex m6">
          <label class="value">
            <span>
              <Text id="time">Time</Text>
            </span>
            <input
              type="datetime-local"
              value={(observation && datetimeFormat(observation.time)) || ''}
              onInput={inputTime}
            />
          </label>
          <IconButton
            title="Use current time"
            icon="clock-now-outline"
            onClick={useCurrentTime}
          />
        </div>
      </div>
      <ObservedForm
        observed={observation?.observed?.[0]}
        startMainExpanded={startMainExpanded}
        onInput={(data) => updateObserved(data, 0)}
      />
    </div>
  );
};

export default FromStore(['settings'])(ObservationForm);
