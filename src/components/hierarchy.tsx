import { h, Fragment } from 'preact';
import { useMemo, useState, useEffect } from 'preact/hooks';

export interface Item {
  id: string;
  title: string;
  label?: string;
  items?: Array<Item>;
}

interface ScoredItem extends Item {
  _score?: number;
}

const sortItems = (items: Array<ScoredItem>, priorityItems: Array<string>) => {
  let score = null;

  items = items.slice();

  for (let i = 0; i < items.length; i++) {
    const item = items[i];
    if (item.items) {
      const result = sortItems(item.items, priorityItems);
      item.items = result.items;
      item._score = result.score;
    } else {
      item._score = priorityItems.indexOf(item.id);
    }
    if (item._score !== -1) {
      score = score === null ? item._score : Math.min(item._score, score);
    }
  }

  items.sort((a, b) => {
    if (b._score === -1 && a._score === -1) {
      return 0;
    }

    if (b._score === -1 || a._score < b._score) {
      return -1;
    }

    return 1;
  });

  return { score: score === null ? -1 : score, items };
};

export default ({
  label,
  items,
  priorityItems,
  value,
  onInput,
  expanded,
  selectable,
  selected,
  itemHeader,
  itemsMap,
  children: itemDetails,
  ...props
}) => {
  const [stateExpanded, setExpanded] = useState([]);

  useEffect(() => {
    setExpanded(Array.isArray(expanded) ? expanded : []);
  }, [expanded]);

  const sortedItems = useMemo(() => {
    if (!priorityItems) {
      return items;
    }

    return sortItems(items, priorityItems).items;
  }, [items, priorityItems]);

  const clickDiv = (event, item) => {
    if (event.defaultPrevented || event.target.nodeName !== 'DIV') {
      return;
    }

    event.preventDefault();

    if (item.items) {
      toggle(item.id);
    } else if (selectable !== false && item.selectable !== false) {
      select(item);
    }
  };

  const toggle = (id) => {
    const index = stateExpanded.indexOf(id);
    if (index === -1) {
      setExpanded([...stateExpanded, id]);
    } else {
      const newExpanded = stateExpanded.slice();
      newExpanded.splice(index, 1);
      setExpanded(newExpanded);
    }
  };

  const select = (item) => {
    setExpanded(stateExpanded);
    if (onInput) {
      onInput({
        value: item.id,
        item
      });
    }
  };

  const makeItems = (currentItems) =>
    currentItems.map((item) => {
      let itemExpanded = stateExpanded.indexOf(item.id) !== -1;
      if (expanded === true) {
        itemExpanded = !itemExpanded;
      }
      const itemSelected = selected?.indexOf(item.id) !== -1;

      const expandButton = item.items ? (
        <button
          aria-expanded={itemExpanded || null}
          onClick={() => toggle(item.id)}
        >
          {itemExpanded ? 'Collapse' : 'Expand'}
        </button>
      ) : null;

      const header = itemHeader ? (
        itemHeader({ item, expandButton })
      ) : (
        <Fragment>
          {selectable !== false && item.selectable !== false ? (
            <button
              class="item"
              aria-selected={itemSelected ? true : null}
              onClick={() => select(item)}
            >
              {item.title || item.label}
            </button>
          ) : (
            item.title || item.label
          )}
          {expandButton}
        </Fragment>
      );

      return (
        <div
          class={`items${itemSelected ? ' selected' : ''}`}
          onClick={(event) => clickDiv(event, item)}
        >
          {header}
          {itemDetails
            ? Array.isArray(itemDetails)
              ? itemDetails.map((render) => render({ item }))
              : itemDetails({ item })
            : null}
          {item.items && itemExpanded ? makeItems(item.items) : null}
        </div>
      );
    });

  return <div class="hierarchy">{makeItems(sortedItems)}</div>;
};
