import { h } from 'preact';
import { FromStore } from '../store';
import { useRef, useState } from 'preact/hooks';
import { getCurrentPosition } from '../lib/location';
import { IconButton } from './icon';
import { siNumber } from '../lib/utils';

const Header = ({
  app,
  settings,
  picks,
  status,
  statusSetStatus,
  statusSetCameraError,
  settingsSetPermission,
  storage,
  storageConnectStorage,
  storageDisconnectStorage,
  storageReauthorizeStorage
}) => {
  const [rsAddress, setRsAddress] = useState(storage.address);
  const rsAddressRef = useRef();
  const [showMenu, setShowMenu] = useState(null);

  /**
   * Toggle the currently shown menu
   *
   * @param {string} menu Menu ID to show
   */
  const toggleMenu = (menu) => {
    if (showMenu === menu) {
      setShowMenu(null);
    } else {
      setShowMenu(menu);
    }
  };

  const rsAddressInput = (event) => {
    setRsAddress(event.target.value);
  };

  const connectRs = (event?: Event) => {
    if (event) {
      event.preventDefault();
    }
    storageConnectStorage(rsAddress);
  };

  const disconnectRs = (event?: Event) => {
    if (event) {
      event.preventDefault();
    }
    storageDisconnectStorage();
  };

  const reauthorizeRs = (event?: Event) => {
    if (event) {
      event.preventDefault();
    }
    storageReauthorizeStorage();
  };

  let storageIcon;
  let storageLabel;
  switch (storage.status) {
    case 'connected':
      storageIcon = 'cloud';
      storageLabel = '';
      break;
    case 'wire-busy':
      storageIcon = 'cloud-sync';
      storageLabel = '';
      break;
    case 'network-offline':
      storageIcon = 'cloud-outline';
      storageLabel = 'Currently offline';
      break;
    case 'sync-done':
      storageIcon = 'cloud-check';
      storageLabel = 'RemoteStorage is in sync';
      break;
    case 'unauthorized':
    case 'error':
      storageIcon = 'cloud-alert';
      storageLabel = 'An error occured';
      break;
    default:
      storageIcon = settings.permissions.storage
        ? 'database'
        : 'database-off-outline';
      storageLabel = settings.permissions.storage
        ? 'Data being stored locally'
        : 'Data not being stored';
  }

  const cameraIcon =
    settings.permissions.camera && !status.cameraError
      ? 'camera'
      : 'camera-off';
  const cameraLabel = status.cameraError
    ? status.cameraError
    : settings.permissions.camera
    ? 'Camera enabled'
    : 'Camera disabled';
  const cameraTitle = status.cameraError
    ? status.cameraError + (status.cameraErrorFatal ? '' : '. Try again')
    : settings.permissions.camera
    ? 'Disable camera'
    : 'Enabled Camera';

  const locationIcon =
    settings.permissions.location !== false
      ? status.findingPosition
        ? 'crosshairs-question'
        : picks.survey && picks.survey.track
        ? status.trackPosition
          ? status.havePosition
            ? 'crosshairs-gps'
            : 'crosshairs-question'
          : 'crosshairs-pause'
        : 'map-marker'
      : 'map-marker-off';
  const locationLabel =
    settings.permissions.location !== false
      ? status.findingPosition
        ? 'Finding current location'
        : picks.survey && picks.survey.track
        ? status.trackPosition
          ? status.havePosition
            ? 'Recording your location'
            : "Don't have your location"
          : 'Position recording paused'
        : 'Location enabled'
      : 'Location disabled';

  const profileIcon = settings.profile ? 'account' : 'account-question-outline';
  const profileLabel = settings.profile ? 'Profile set' : 'Profile not set';

  const clickLocation = () => {
    if (settings.permissions.location === false) {
      settingsSetPermission('location', null);
      getCurrentPosition()
        .catch((error) => {
          return error.code !== 1;
        })
        .then((result) => {
          if (result !== false) {
            settingsSetPermission('location', true);
          }
        });
    } else if (settings.permissions.location && picks.survey?.track) {
      statusSetStatus('trackPosition', !status.trackPosition);
    }
  };

  const clickCamera = () => {
    if (status.cameraError) {
      statusSetCameraError(null);
    }
    if (!settings.permissions.camera) {
      settingsSetPermission('camera', true);
    } else {
      settingsSetPermission('camera', false);
    }
  };

  return (
    <header>
      <div class="logo"></div>
      <h1>
        <a href="/add">{app.name}</a>
      </h1>
      <div class="actions">
        {app.surveys ? (
          <a href="/survey">{picks.survey ? 'Survey' : 'Start Survey'}</a>
        ) : null}
      </div>
      <IconButton
        title="Storage Settings"
        double
        icon={storageIcon}
        label={storageLabel}
        onClick={() => toggleMenu('storage')}
      />
      <IconButton
        title={cameraTitle}
        double
        icon={cameraIcon}
        label={cameraLabel}
        onClick={clickCamera}
        disabled={status.cameraErrorFatal}
      />
      <IconButton
        double
        icon={locationIcon}
        label={locationLabel}
        onClick={clickLocation}
      />
      <IconButton double icon={profileIcon} label={profileLabel} />
      {showMenu === 'storage' ? (
        <section class="menu">
          <h2>Storage Settings</h2>
          <h3>Browser Storage</h3>
          {storage?.persisted ? null : (
            <p class="warning">
              Data is not persisted in your browser and may be removed at any
              time.
            </p>
          )}
          {storage?.estimate ? (
            <p>
              Currently using: {siNumber(storage?.estimate?.usage)}B of{' '}
              {siNumber(storage?.estimate?.quota)}B
            </p>
          ) : (
            <p>Current usage unknown</p>
          )}
          <h3>Remote Storage</h3>
          {!storage.connected ? (
            <div>
              <p>
                Store your surveys on your own{' '}
                <a href="https://remotestorage.io/" target="_blank">
                  remoteStorage
                </a>{' '}
                server.
              </p>
              <form>
                <label class={rsAddress ? 'value' : null}>
                  <span>RemoteStorage Address</span>
                  <input
                    type="email"
                    placeholder="user@example.com"
                    value={rsAddress}
                    onInput={rsAddressInput}
                    ref={rsAddressRef}
                  />
                </label>
                <div class="right">
                  <button
                    class="remoteStorage"
                    disabled={
                      !rsAddress || !rsAddressRef.current?.validity.valid
                    }
                    onClick={connectRs}
                  >
                    Connect
                  </button>
                </div>
              </form>
              {/*<p>
              Sign up to use our remoteStorage server (storage.gfon.org)
            </p>
            <form>
              <label>
                <span>Username</span>
                <input/>
              </label>
              <label>
                <span>Password</span>
                <input/>
              </label>
              <div class="right">
                <button>Sign Up</button>
              </div>
            </form> */}
            </div>
          ) : (
            <div>
              Connected to {storage.address}
              <button onClick={disconnectRs}>Disconnect</button>
              {storage.status === 'unauthorized' ? (
                <button onClick={reauthorizeRs}>Reauthorize</button>
              ) : null}
              <div>
                Status: {storage.status.toUpperCase()}
                {storage.statusMessage ? ': ' + storage.statusMessage : ''}
              </div>
            </div>
          )}
          <h3>Organisation Submission</h3>
        </section>
      ) : null}
      <nav>
        <IconButton
          double
          icon="bars"
          label="Menu"
          aria-expanded={showMenu === 'nav'}
          onClick={() => toggleMenu('nav')}
        />
        <ul class="menu" hidden={showMenu !== 'nav'}>
          <li>
            <a href="/add" onClick={() => toggleMenu('nav')}>
              Add
            </a>
          </li>
          <li>
            <a href="/list" onClick={() => toggleMenu('nav')}>
              History
            </a>
          </li>
          <li>
            <a href="/survey" onClick={() => toggleMenu('nav')}>
              Survey
            </a>
          </li>
        </ul>
      </nav>
      {showMenu ? <div class="cover" onClick={() => toggleMenu(null)} /> : null}
    </header>
  );
};

export default FromStore(['app', 'settings', 'status', 'picks', 'storage'])(
  Header
);
