import { h, createRef } from 'preact';
import { useEffect } from 'preact/hooks';
import { useRefState, usePropRef } from '../lib/hooks';
import { FromStore } from '../store';
import { baseId } from '../lib/id';

import { IntlProvider, Text } from 'preact-i18n';
import {
  getCurrentPosition,
  watchPosition,
  stopWatch,
  distance
} from '../lib/location';
import { getSurveyTrack } from '../lib/survey';
import CameraCapture from './camera-capture';
import Map from '../components/map';
import ObservationForm from './observationForm';

import { mcsUrl } from '../taxon/mcsObjects';

const LitterPick = ({
  picks,
  recentAdd,
  settings,
  settingsSetPermission,
  picksLitterAdd,
  picksSurveyUpdate,
  picksSurveyAddTrackPosition,
  status,
  statusHavePosition,
  statusSetCameraError,
  statusSetFindingPosition,
  statusSetPositionError
}) => {
  const camera = createRef();
  const currentPositionRef = useRefState(null)[2];
  const [litter, , litterRef] = useRefState(null);
  const [grabMapClick, setGrabMapClick, grabMapClickRef] = useRefState(false);
  const [positionWatch, setPositionWatch, positionWatchRef] = useRefState(null);
  const surveyRef = usePropRef(picks.survey);
  const statusHavePositionRef = usePropRef(statusHavePosition);
  const statusSetFindingPositionRef = usePropRef(statusSetFindingPosition);
  const statusSetPositionErrorRef = usePropRef(statusSetPositionError);
  const settingsSetPermissionRef = usePropRef(settingsSetPermission);
  const cameraEnabled = settings.permissions.camera && !status.cameraError;

  const updatePosition = (position) => {
    if (!surveyRef.current) {
      return;
    }
    const surveyTrack = getSurveyTrack(surveyRef.current);
    if (
      !surveyTrack?.length ||
      distance(surveyTrack[surveyTrack.length - 1], position) > 5
    ) {
      picksSurveyAddTrackPosition(position);
    }
    if (!settings.permissions.location) {
      settingsSetPermission('location', true);
    }
    settingsSetPermissionRef.current('location', true);
    statusHavePositionRef.current();
    currentPositionRef.set(position);
  };

  const positionError = (error) => {
    switch (error.code) {
      case GeolocationPositionError.TIMEOUT:
        statusHavePositionRef.current(false);
        break;
      case GeolocationPositionError.PERMISSION_DENIED:
        statusSetPositionErrorRef.current(
          'Permission denied to current position',
          false
        );
        break;
      case GeolocationPositionError.POSITION_UNAVAILABLE:
        statusSetPositionErrorRef.current(
          'Could not get position due to internal error',
          false
        );
        break;
    }
  };

  useEffect(() => {
    if (settings.permissions.storage === null && navigator?.storage?.persist) {
      navigator.storage.persist().then((result) => {
        settingsSetPermissionRef.current('storage', result);
      });
    }
  }, [settings.permissions.storage]);

  useEffect(() => {
    if (
      picks.survey &&
      picks.survey.track &&
      status.trackPosition &&
      positionWatch === null
    ) {
      setPositionWatch(watchPosition(updatePosition, positionError));
    } else if (
      positionWatch !== null &&
      (!picks.survey || !picks.survey.track || !status.trackPosition)
    ) {
      stopWatch(positionWatch);
      setPositionWatch(null);
    }
  }, [picks.survey, positionWatch, status.trackPosition]);

  useEffect(() => {
    return () => {
      if (positionWatchRef.value) {
        stopWatch(positionWatchRef.value);
        setPositionWatch(null);
      }
    };
  }, []);

  const updateLitter = (litterUpdate) => {
    grabMapClickRef.set(false);
    if (!litterRef.value) {
      newLitter(litterUpdate);
    } else {
      const newValue = {
        ...litterRef.value,
        ...litterUpdate
      };
      litterRef.set(newValue);
    }
  };

  const useCurrentPosition = () => {
    statusSetFindingPosition(true);
    getCurrentPosition()
      .then((position) => {
        updateLitter({
          location: [position]
        });
        if (!settings.permissions.location) {
          settingsSetPermissionRef.current('location', true);
        }
        updatePosition(position);
      })
      .finally(() => {
        statusSetFindingPositionRef.current(false);
      });
  };

  const handleCurrentPosition = () => {
    const promise = getCurrentPosition();

    promise.then((position) => {
      updatePosition(position);
    });

    return promise;
  };

  const clickMap = (event) => {
    if (grabMapClickRef.value) {
      updateLitter({
        location: [
          {
            latitude: Number(event.latlng.lat.toFixed(6)),
            longitude: Number(event.latlng.lng.toFixed(6))
          }
        ]
      });
      grabMapClickRef.set(false);
    }
  };

  const cameraError = (error: DOMException) => {
    switch (error.name) {
      case 'NotAllowedError':
        settingsSetPermission('camera', false);
        statusSetCameraError('Camera access permission denied');
        break;
      case 'NotFoundError':
        statusSetCameraError('No camera found', true);
        break;
      default:
        settingsSetPermission('camera', false);
        statusSetCameraError(error.message);
        break;
    }
  };

  const togglePositionFromMap = () => {
    setGrabMapClick(!grabMapClick);
  };

  const newLitter = (newLitter = {}) => {
    if (!newLitter) {
      litterRef.set(null);
      return;
    }

    newLitter = {
      time: new Date(),
      ...newLitter
    };

    if (!newLitter.location) {
      useCurrentPosition();
    }

    litterRef.set(newLitter);
  };

  const saveLitter = () => {
    if (litterRef.value) {
      picksLitterAdd(litterRef.value);

      const observed = litterRef.value.observed;
      if (observed?.length) {
        for (let i = 0; i < observed.length; i++) {
          if (observed[i]['@id']?.startsWith(mcsUrl)) {
            recentAdd('mcs', observed[i]['@id'].slice(mcsUrl.length));
          } else if (observed[i]['@id']) {
            recentAdd('type', baseId(observed[i]['@id']));
          }

          if (observed[i].material?.['@id']) {
            recentAdd('material', baseId(observed[i].material['@id']));
          }

          if (observed[i].brand?.['@id']) {
            recentAdd('brand', observed[i].brand['@id']);
          }

          if (observed[i].size?.['@id']) {
            recentAdd('size', baseId(observed[i].size['@id']));
          }
        }
      }

      litterRef.set(null);
    }
  };

  const takePhoto = () => {
    if (camera.current && camera.current.takePhoto) {
      camera.current.takePhoto().then((blob) => {
        if (litterRef.value) {
          litterRef.set({
            ...litterRef.value,
            media: [
              ...(litterRef.value.media || []),
              {
                mimetype: blob.type,
                blob,
                url: URL.createObjectURL(blob)
              }
            ]
          });
        }
      });
    }
  };

  const newLitterPhoto = () => {
    saveLitter();
    newLitter();

    takePhoto();
  };

  return (
    <IntlProvider scope="litterPick">
      <section class="litterPick">
        <div class="imagery">
          {cameraEnabled ? (
            <CameraCapture
              ref={camera}
              onError={cameraError}
              enabled={cameraEnabled}
            />
          ) : null}
          <Map
            class={`map${grabMapClick ? ' grab' : ''}`}
            onClick={grabMapClick ? clickMap : null}
            currentPosition={currentPositionRef.value}
            markedPosition={litterRef.value?.location?.[0]}
          />
        </div>
        <ObservationForm
          observation={litter}
          startMainExpanded={true}
          onInput={updateLitter}
          grabMapClick={grabMapClick}
          getCurrentPosition={handleCurrentPosition}
          onRequestMapPosition={togglePositionFromMap}
          alwaysShowMedia={cameraEnabled}
        />
        <div class="actions">
          <button onClick={saveLitter} disabled={!litter?.observed?.[0]}>
            <Text id="save">Save</Text>
          </button>
          {cameraEnabled ? (
            <button onClick={takePhoto} disabled={!litter}>
              <Text id="anotherPhotoButton">Add a photo</Text>
            </button>
          ) : null}
          {cameraEnabled ? (
            <button
              onClick={newLitterPhoto}
              disabled={litter && !litter?.observed?.[0]}
            >
              <Text id="newLitterButton">
                {litter ? (
                  <Text id="saveAndNew">Save and take new photo</Text>
                ) : (
                  <Text id="new">Take new litter photo</Text>
                )}
              </Text>
            </button>
          ) : null}
        </div>
      </section>
    </IntlProvider>
  );
};

export default FromStore(['settings', 'recent', 'picks', 'status'])(LitterPick);
