import { h } from 'preact';
import { Text } from 'preact-i18n';
import JSXStyle from 'styled-jsx/style';

// TODO Need i18n for labels
const allTags = {
  material: [
    {
      tag: 'plastic',
      label: 'Plastic',
      children: [],
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-1',
      label: '1 - PET',
      title: 'PET (Polyethylene Terephthalate)',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-2',
      label: '2 - HDPE',
      title: 'HDPE (High Density Polyethylene)',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-3',
      label: '3 - PVC',
      title: 'PVC (Vinyl)',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-4',
      label: '4 - LDPE',
      title: 'LDPE (Low Density Polyethylene)',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-5',
      label: '5 - PP',
      title: 'PP Polypropylene()',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-6',
      label: '6 - PS',
      title: 'PS (Polystyrene)',
      commonType: [],
      commonBrand: []
    },
    {
      tag: 'plastic-7',
      label: '7 - Other',
      commonType: [],
      commonBrand: []
    }
  ],
  type: [
    {
      tag: 'straw',
      label: 'Straw',
      commonMaterial: [0]
    },
    {
      tag: 'bottle',
      label: 'Bottle',
      commonMaterial: [0, 2]
      commonBrand: [0]
    }
  ],
  brand: [
    {
      tag: 'coca-cola',
      alias: ['coke', 'fanta', 'sprite'],
      label: 'Coca Cola',
      commonMaterial: [0, 2],
      commonType: [0]
    }
  ]
};


const Tags = ({ tags, onChangeTags }) => {
  const clickTag = (tag) => {
    console.log('click tag', tag);
    let newTags;
    if (tags) {
      newTags = [ ...tags ];
      const index = newTags.indexOf(tag);
      if (index === -1) {
        newTags.push(tag);
      } else {
        newTags.splice(index, 1);
      }
    } else {
      newTags = [ tag ];
    }
    if (onChangeTags) {
      onChangeTags(newTags);
    }
  };

  return (
    <div class="tags">
      <div class="matieral">
        <h2><Text id="material">Material</Text></h2>
        { allTags.material.map((tag) => (
          <button onClick={() => clickTag(tag.tag)}>{ tag.label }</button>
        )) }
      </div>
      <div class="type">
        <h2><Text id="type">Type</Text></h2>
        { allTags.type.map((tag) => (
          <button onClick={() => clickTag(tag.tag)}>{ tag.label }</button>
        )) }
      </div>
      <div class="brand">
        <h2><Text id="brand">Brand</Text></h2>
        { allTags.brand.map((tag) => (
          <button onClick={() => clickTag(tag.tag)}>{ tag.label }</button>
        )) }
      </div>
      <JSXStyle id="tags">{`
        .tags button {
          border: none;
          margin: 2px 5px;
          padding: 5px 10px;
          background: #ccc;
          border-radius: 5px;
        }
        .tags h2 {
          display: inline-block;
          font-size: 1em;
        }
      `}</JSXStyle>
    </div>
  );
};

export default Tags;
