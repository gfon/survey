import { sessionGet, sessionSet } from './utils';

export interface Item {
  id: string;
  title: string;
  label?: string;
  [key: string]: any;
}

export interface Recent {
  [key: string]: Array<Item>;
}

export const initialState: Recent = sessionGet('recent') || {};

export const actions: Actions<Recent> = {
  recentClear: (state: Recent, key?: string) => {
    let newState;
    if (key) {
      newState = {
        ...state,
        [key]: null
      };
    } else {
      newState = {};
    }

    sessionSet('recent', newState);
    return newState;
  },
  recentAdd: (state: Recent, key: string, item: Item) => {
    let newState;
    if (!state[key]) {
      newState = {
        ...state,
        [key]: [item]
      };
    } else {
      const index = state[key].indexOf(item);
      const newItems = state[key].slice();

      if (index !== -1) {
        newItems.splice(index, 1);
      }
      newItems.unshift(item);

      newState = {
        ...state,
        [key]: newItems.slice(0, 10)
      };
    }

    sessionSet('recent', newState);
    return newState;
  }
};
