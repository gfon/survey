import { h } from 'preact';
import createUnistore from 'unistore';
import { Provider, connect } from 'unistore/preact';
import devTools from 'unistore/devtools';

import * as user from './user';
import * as settings from './settings';
import * as picks from './picks';
import * as recent from './recent';
import * as status from './status';
import * as storage from './storage';
import * as app from './app';

const initialState = {};
const objectActions = {};
const actionFunctions = {};

const createMapWrappers = (store, key, rawActions, actions) => {
  Object.entries(rawActions).forEach(([actionName, action]) => {
    actions[actionName] = (state, ...args) => {
      const result = action(state[key], ...args);

      if (result instanceof Promise) {
        return result.then((value) => ({
          ...store.getState(),
          [key]: value
        }));
      }

      return {
        ...state,
        [key]: result
      };
    };
  });
};

const actionsCache = new Map();

const createModuleStore = (store, name: string) => {
  return {
    ...store,
    dispatch: (action: string, ...args) => {
      const actions = actionsCache.get(store);

      if (!actions) {
        throw new Error('Unknown store');
      }

      if (!actions[action]) {
        throw new Error(`Unknown action ${action}`);
      }

      const state = store.getState();
      const result = actions[action](state[name], ...args);

      if (result instanceof Promise) {
        return result.then((newState) => {
          store.setState(newState);
        });
      } else {
        store.setState({
          ...state,
          [name]: result
        });
      }
    }
  };
};

Object.entries({ app, user, settings, picks, recent, status, storage }).forEach(
  ([key, store]) => {
    initialState[key] = store.initialState;

    if (typeof store.actions === 'function') {
      if (process.env.NODE_ENV !== 'production') {
        const actions = store.actions(null);

        Object.keys(actions).forEach((actionName) => {
          if (!actionName.startsWith(key)) {
            console.warn(
              `Action '${actionName}' for '${key}' should start with ${key}`
            );
          }
        });
      }

      actionFunctions[key] = store.actions;
    } else {
      if (process.env.NODE_ENV !== 'production') {
        Object.entries(store.actions).forEach(([actionName, action]) => {
          if (!key.startsWith(key)) {
            console.warn(
              `Action '${actionName}' for '${key}' should start with ${key}`
            );
          }
        });
      }
      objectActions[key] = store.actions;
    }
  }
);

const actionFunction = (store) => {
  if (actionsCache.has(store)) {
    return actionsCache.get(store);
  }

  const actions = {};

  Object.entries(objectActions).forEach(([key, keyActions]) => {
    createMapWrappers(store, key, keyActions, actions);
  });

  Object.entries(actionFunctions).forEach(([key, action]) => {
    const moduleStore = createModuleStore(store, key);
    const createdActions = action(moduleStore);

    createMapWrappers(moduleStore, key, createdActions, actions);
  });

  actionsCache.set(store, actions);
  return actions;
};

const globalStoreInstance = createUnistore(initialState);

/**
 * Create a function to connect a component to a certain part of the store
 *
 * @param use Part(s) of the store to connect to
 */
export const FromStore = (use: string | Array<string>) => {
  return connect(use, actionFunction);
};

/**
 * The store components
 */
export const Store = ({ children }) =>
  h(
    Provider,
    {
      store: globalStoreInstance
    },
    children
  );

export default process.env.NODE_ENV === 'production'
  ? globalStoreInstance
  : devTools(globalStoreInstance);
