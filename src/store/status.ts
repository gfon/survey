export interface Status {
  trackPosition?: boolean;
  havePosition: boolean;
  findingPosition: boolean;
  positionTimeout?: number;
  positionError?: string;
  positionErrorFatal: boolean;
  cameraError?: string;
  cameraErrorFatal?: boolean;
}

export const initialState: Status = {
  trackPosition: true,
  havePosition: false,
  findingPosition: false,
  positionError: null,

  positionTimeout: null,
  cameraError: null,
  cameraErrorFatal: false
};

export const actions = (store): Actions<Status> => ({
  statusSetStatus: (state: Status, status: string, value: any) => {
    return {
      ...state,
      [status]: value
    };
  },
  statusHavePosition: (state: Status, havePosition: boolean = true) => {
    if (state.positionTimeout !== null) {
      clearTimeout(state.positionTimeout);
    }

    if (!havePosition) {
      return {
        ...state,
        havePosition,
        positionTimeout: null
      };
    }

    return {
      ...state,
      havePosition: true,
      positionTimeout: setTimeout(() => {
        const currentState = store.getState();
        store.setState({
          ...currentState,
          status: {
            ...currentState.status,
            havePosition: false,
            positionTimeout: null
          }
        });
      }, 60000)
    };
  },
  statusSetFindingPosition: (state: Status, findingPosition: boolean) => ({
    ...state,
    findingPosition
  }),
  statusSetPositionError: (state: Status, error: string, fatal: boolean = false) => ({
    ...state,
    positionError: error,
    positionErrorFatal: fatal
  }),
  statusSetCameraError: (
    state: Status,
    message: string,
    fatal: boolean = false
  ) => {
    return {
      ...state,
      cameraError: message,
      cameraErrorFatal: fatal
    };
  }
});
