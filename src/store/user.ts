import { Location, Bounds, cloneBounds } from '../lib/location';
import { Actions } from '../typings/store';
import { gotLocalStorage, get, set } from './utils';

export interface User {
  /// Whether or not the user has been shown the welcome screen
  welcomed: boolean;
  /// The decision of the user to allow ServiceWorkers
  allowServiceWorker: boolean | null;
  /// Whether the home bounds have been set by the user
  homeBoundsSet: boolean;
  /// The home bounds of the user
  homeBounds: Bounds | null;
}

export const initialState: User = {
  welcomed: false,
  allowServiceWorker: null,
  homeBoundsSet: false,
  homeBounds: null
};


if (gotLocalStorage) {
  initialState.welcomed = get('welcomed') || false;
  initialState.allowServiceWorker = get('allowServiceWorker') || false;
}

export const actions: Actions<User> = {
  userSetWelcomed: (state: User) => {
    console.log('userSetWelcomed called', state);
    if (gotLocalStorage) {
      set('welcomed', true);
    }
    return {
      ...state,
      welcomed: true
    };
  },
  userSetHomeBounds: (state: User, bounds: Bounds | null) => ({
    ...state,
    homeBoundsSet: Boolean(bounds),
    bounds
  }),
  /**
   * Add a location to the home bounds if the user hasn't set their
   * home bounds
   *
   * @param newLocation The location to add to the home bounds
   */
  userAddToHomeBounds: (state: User, newLocation: Location) => {
    if (!state.homeBoundsSet) {
      let homeBounds: Bounds;
      if (state.homeBounds) {
        homeBounds = {
          minLatitude: Math.min(state.homeBounds.minLatitude, newLocation.latitude),
          minLongitude: Math.min(state.homeBounds.minLongitude, newLocation.longitude),
          maxLatitude: Math.max(state.homeBounds.maxLatitude, newLocation.latitude),
          maxLongitude: Math.max(state.homeBounds.maxLongitude, newLocation.longitude)
        };
      } else {
        homeBounds = {
          minLatitude: newLocation.latitude,
          minLongitude: newLocation.longitude,
          maxLatitude: newLocation.latitude,
          maxLongitude: newLocation.longitude
        };
      }

      return {
        ...state,
        homeBounds
      };
    }

    return state;
  }
};
