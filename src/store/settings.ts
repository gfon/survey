import { gotLocalStorage, get, set } from './utils';

export interface CloudInstance {}

export interface Settings {
  permissions: {
    storage: boolean;
    camera: boolean;
    location: boolean;
    serviceWorker: boolean;
  };
  cloud: null | Array<CloudInstance>;
}

const defaultState: Settings = {
  permissions: {
    storage: null,
    camera: true,
    location: true,
    serviceWorker: null
  },
  cloud: null
};

// TODO Add checking of settings from local storage
export const initialState: Settings =
  (gotLocalStorage && get('settings')) || defaultState;

export const actions: Actions<Settings> = {
  settingsSetPermission: (
    state: Settings,
    permission: string,
    value: boolean | null
  ) => {
    state = {
      ...state,
      permissions: {
        ...state.permissions,
        [permission]: value
      }
    };

    set('settings', state);
    return state;
  }
};
