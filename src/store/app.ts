export interface AppState {
  name: string;
  allowObservationsOutsideSurveys: boolean;
  surveys?: any[];
}

const defaultState: AppState = {
  name: 'Litter',
  allowObservationsOutsideSurveys: true,
  surveys: []
};

export const initialState: AppState = defaultState;

export const actions: Actions<AppState> = (store) => ({});
