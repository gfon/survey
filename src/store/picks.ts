import { Survey, Litter } from '../typings/litter';
import { Actions } from '../typings/store';

import { sessionSet, sessionGet } from './utils';
import { loadLitterImages } from '../lib/litter';
import {
  storeLitter,
  storeSurvey,
  surveyGet,
  litterCount,
  litterGetForSurvey,
  nonSurveyLitterAdd
} from '../lib/storage';

export interface LitterState {
  litter?: Array<Litter>;
  survey?: Survey;
  imagesLoaded: boolean;
  history?: {
    surveysCount: number;
    surveys: Array<Survey>;
    litterCount: number;
    otherLitterCount: number;
    litter?: {
      [id: string]: Array<Litter>;
    };
  };
}

const defaultState: LitterState = {
  litter: sessionGet('litter') || [],
  imagesLoaded: false,
  survey: sessionGet('survey') || null,
  history: null
};

if (defaultState.survey?.startTime) {
  defaultState.survey.startTime = new Date(defaultState.survey.startTime);
}
if (defaultState.survey?.endTime) {
  defaultState.survey.endTime = new Date(defaultState.survey.endTime);
}

export const initialState: LitterState = defaultState;

//TODO Change to function to so can get latest state from store
export const actions: Actions<LitterState> = (store) => ({
  picksSurveySet: (state: LitterState, survey: any) => {
    if (survey) {
      return storeSurvey(survey).then((item) => {
        sessionSet('survey', item);
        return {
          ...state,
          survey: item
        };
      });
    }

    sessionSet('survey', null);
    return {
      ...state,
      survey
    };
  },
  picksSurveyUpdate: (state: LitterState, update: any) => {
    if (state.survey && update) {
      return storeSurvey({
        ...state.survey,
        ...update
      }).then((survey) => {
        sessionSet('survey', survey);

        return {
          ...state,
          survey
        };
      });
    }

    return state;
  },
  picksSurveyAddTrackPosition: (state: LitterState, position: Position) => {
    let survey;
    if (state.survey) {
      if (state.survey.location) {
        const index = state.survey.location.findIndex(
          (location) => location.type === 'track'
        );

        if (index === -1) {
          survey = {
            ...state.survey,
            location: [
              ...state.survey.location,
              {
                type: 'track',
                track: [position]
              }
            ]
          };
        } else {
          const newTrack = state.survey.location[index].track;
          newTrack.push(position);
          survey = {
            ...state.survey,
            location: [...state.survey.location]
          };
          survey.location[index] = {
            type: 'track',
            track: newTrack
          };
        }
      } else {
        survey = {
          ...state.survey,
          location: [
            {
              type: 'track',
              track: [position]
            }
          ]
        };
      }

      return storeSurvey(survey).then((item) => {
        sessionSet('survey', item);
        return {
          ...state,
          survey: item
        };
      });
    }
  },
  picksLitterAdd: (state: LitterState, litterItem: any) => {
    return storeLitter({
      ...litterItem,
      survey: state.survey?.['@id'] || undefined
    })
      .then((item) => {
        if (state.survey) {
          // Add litter to survey
          return storeSurvey({
            ...state.survey,
            observations: [...(state.survey.observations || []), item['@id']]
          }).then((survey) => {
            sessionSet('survey', survey);
            return [item, survey];
          });
        } else {
          // Add litter to non-survey litter
          return nonSurveyLitterAdd(item['@id']).then(() => [item, null]);
        }
      })
      .then(([item, survey]) => {
        litterItem['@id'] = item['@id'];
        sessionSet('litter', [...(sessionGet('litter') || []), item]);

        if (item.media?.length) {
          for (let i = 0; i < item.media.length; i++) {
            litterItem.media[i]['@id'] = item.media[i]['@id'];
          }
        }

        return {
          ...state,
          litter: [...state.litter, litterItem],
          survey
        };
      });
  },
  picksLitterUpdate: (state: LitterState, litterItem: any) => {
    return storeLitter(litterItem).then((item) => {
      // Find the litter item in the lists
      let newState = null;
      if (state.litter?.length) {
        for (let i = 0; i < state.litter.length; i++) {
          if (state.litter[i]['@id'] === item['@id']) {
            if (!newState) {
              newState = { ...state };
            }
            newState.litter = [...newState.litter];
            newState.litter[i] = litterItem;
            const sessionLitter = sessionGet('litter');
            sessionLitter[i] = item;
            sessionSet('litter', sessionLitter);
            break;
          }
        }
      }

      if (state.history) {
        if (litterItem.survey) {
          let surveyLitter = state.history.litter?.[litterItem.survey];
          if (surveyLitter) {
            for (let i = 0; i < surveyLitter.length; i++) {
              if (surveyLitter[i]['@id'] === item['@id']) {
                if (!newState) {
                  newState = { ...state };
                }
                surveyLitter = [...surveyLitter];
                surveyLitter[i] = litterItem;
                newState.history = {
                  ...newState.history,
                  litter: {
                    ...newState.history.litter,
                    [litterItem.survey]: surveyLitter
                  }
                };
                break;
              }
            }
          }
        } else {
          if (state.history.otherLitter?.length) {
            for (let i = 0; i < state.history.otherLitter.length; i++) {
              if (state.history.otherLitter[i]['@id'] === item['@id']) {
                if (!newState) {
                  newState = { ...state };
                }
                const otherLitter = [...state.history.otherLitter];
                otherLitter[i] = litterItem;
                newState.history = {
                  ...newState.history,
                  otherLitter
                };
                break;
              }
            }
          }
        }
      }

      return newState || state;
    });
  },
  picksLitterClear: (state: LitterState) => {
    // Revoke any media urls
    if (state.litter?.length) {
      for (let i = 0; i < state.litter.length; i++) {
        if (state.litter[i].media?.length) {
          for (let j = 0; j < state.litter[i].media.length; j++) {
            if (state.litter[i].media[j].url) {
              URL.revokeObjectURL(state.litter[i].media[j].url);
            }
          }
        }
      }
    }

    sessionSet('litter', []);
    return {
      ...state,
      litter: []
    };
  },
  picksImagesLoad: (state: LitterState) => {
    if (state.imagesLoaded) {
      return state;
    }

    const newState = {
      ...state,
      imagesLoaded: true
    };

    if (state.litter?.length) {
      return loadLitterImages(state.litter).then((litter) => {
        newState.litter = litter;

        return newState;
      });
    }

    return newState;
  },
  picksGetHistory: (state: LitterState) => {
    return Promise.all([surveyGet(), litterCount()]).then(
      ([surveys, litterCount]) => {
        const counts = {
          '': litterCount
        };

        surveys = Object.values(surveys);

        surveys.sort((a, b) => {
          if (a.startTime < b.startTime) {
            return -1;
          } else if (a.startTime > b.startTime) {
            return 1;
          }

          return 0;
        });

        for (let i = 0; i < surveys.length; i++) {
          counts[surveys[i]['@id']] = surveys[i].observations?.length;
          counts[''] -= surveys[i].observations?.length;
        }

        return {
          ...store.getState().picks,
          history: {
            surveysCount: surveys.length,
            surveys: surveys.map((item) => ({
              ...item,
              startTime: item.startTime && new Date(item.startTime),
              endTime: item.endTime && new Date(item.endTime)
            })),
            litterCount,
            otherLitterCount: counts[''],
            litter: {}
          }
        };
      }
    );
  },
  picksGetSurveyLitter: (state: LitterState, surveyId: string) => {
    let promise = null;
    if (!state.history) {
      promise = store.dispatch('picksGetHistory');
    }

    if (!state.history?.litter?.[surveyId]) {
      if (promise) {
        promise.then(() => litterGetForSurvey(surveyId));
      } else {
        promise = litterGetForSurvey(surveyId);
      }

      return promise.then((litter) => {
        const currentState = store.getState();

        return {
          ...currentState.picks,
          history: {
            ...currentState.picks?.history,
            litter: {
              ...currentState.picks?.history?.litter,
              [surveyId]: litter
            }
          }
        };
      });
    }

    if (promise) {
      return promise.then(() => store.getState().history);
    }

    return state;
  }
});
