export const gotLocalStorage =
  typeof window !== 'undefined' && typeof localStorage !== 'undefined';

/**
 * Get a value from localStorage
 *
 * @param key Key of the value to retrieve from localStorage
 */
export const get = (key) => {
  const value = localStorage.getItem(key);

  if (value !== null) {
    try {
      return JSON.parse(value);
    } catch {
      return null;
    }
  }

  return null;
};

/**
 * Set a settings value
 *
 * @param key Key of the value to set
 * @param value New value to be set
 */
export const set = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

/**
 * Get a value from sessionStorage
 *
 * @param key Key of the value to retrieve from localStorage
 */
export const sessionGet = (key) => {
  const value = sessionStorage.getItem(key);

  if (value !== null) {
    try {
      return JSON.parse(value);
    } catch {
      return null;
    }
  }

  return null;
};

/**
 * Set a settings value
 *
 * @param key Key of the value to set
 * @param value New value to be set
 */
export const sessionSet = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value));
};
