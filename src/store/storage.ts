import { remoteStorage } from '../lib/storage';

export interface Storage {
  connected?: boolean;
  address?: string;
  status?: string;
  estimate?: {
    usage: number;
    quota: number;
  };
  persisted?: boolean;
}

export const initialState: Storage = {
  connected: null,
  address: null,
  status: 'initialising',
  statusMessage: null,
  estimate: null,
  persisted: null
};

export const actions = (store): Actions<Storage> => {
  const checkPersisted = () => navigator?.storage?.persisted?.().then((persisted) => {
    const state = store.getState();
    store.setState({
      ...state,
      storage: {
        ...state.storage,
        persisted
      }
    });
  });

  const updateEstimate = () => navigator?.storage?.estimate?.().then((estimate) => {
    const state = store.getState();
    store.setState({
      ...state,
      storage: {
        ...state.storage,
        estimate
      }
    });
  });

  if (store) {
    updateEstimate();
    checkPersisted();

    let promise;

    if (remoteStorage instanceof Promise) {
      promise = remoteStorage;
    } else {
      promise = Promise.resolve();
    }

    promise.then(() => {
      const updateStatus = (newStatus: string) => (event) => {
        let statusMessage = null;
        if (newStatus === 'error') {
          if (event.name === 'Unauthorized') {
            statusMessage = event.message;
            newStatus = 'unauthorized';
          }
        }

        const connected = ['not-connected', 'disconnected'].indexOf(newStatus) !== -1
            ? false : newStatus === 'ready' ? null : true;
        const state = store.getState();
        const newState = {
          ...state,
          storage: {
            ...state.storage,
            connected,
            status: newStatus,
            statusMessage
          }
        };
        if (connected) {
          newState.storage.address = remoteStorage.remote.userAddress;
        }
        store.setState(newState);
      };

      remoteStorage.on('ready', updateStatus('ready'));
      remoteStorage.on('not-connected', updateStatus('not-connected'));
      remoteStorage.on('connected', updateStatus('connected'));
      // TODO add disconnect functionality - remove id urls
      remoteStorage.on('disconnected', updateStatus('not-connected'));
      remoteStorage.on('error', updateStatus('error'));
      remoteStorage.on('wire-busy', updateStatus('wire-busy'));
      remoteStorage.on('wire-done', updateStatus('connected'));
      remoteStorage.on('sync-done', updateStatus('sync-done'));
      remoteStorage.on('network-offline', updateStatus('network-offline'));
      remoteStorage.on('network-online', updateStatus('connected'));
    });
  }

  return {
    storageSetStatus: (state: Storage, status: string) => {
      return {
        ...state,
        status
      };
    },
    storageSetConnected: (state: Storage, connected: boolean) => {
      return {
        ...state,
        connected
      };
    },
    storageRecheckPersisted: (state: Storage) => {
      checkPersisted();
      return state;
    },
    storageRecheckEstimate: (state: Storage) => {
      updateEstimate();
      return state;
    },
    storageConnectStorage: (state: Storage, userAddress: string, userToken?: string) => {
      if (state.connected) {
        throw new Error('Already connected to a remoteStorage instance');
      }

      remoteStorage.connect(userAddress, userToken);
    },
    storageDisconnectStorage: (state: Storage) => {
      if (!state.connected) {
        return;
      }

      remoteStorage.disconnect();
    },
    storageReauthorizeStorage: (state: Storage) => {
      if (!state.connected) {
        throw new Error('Not connected to a remoteStorage instance');
      }

      remoteStorage.disconnect();
      remoteStorage.connect(state.address);
    }
  };
};
