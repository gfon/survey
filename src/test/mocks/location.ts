export { toLatLng } from '../../lib/location';

const location = [51.32444, -0.569379];
let lastUpdate = new Date();

setInterval(() => {
  location[0] += 0.00002;
  location[1] += 0.00005;
  lastUpdate = new Date();
}, 5000);

const makePosition = () => ({
  time: new Date(),
  age: new Date().getTime() - lastUpdate.getTime(),
  latitude: location[0],
  longitude: location[1],
  accuracy: 5
});

export const getCurrentPosition = () => {
  return Promise.resolve(makePosition());
};

export const watchPosition = (callback, error) => {
  const id = setInterval(() => {
    callback(makePosition());
  }, 5000);
  setTimeout(() => {
    callback(makePosition());
  }, 0);
  return id;
};

export const stopWatch = (id) => {
  clearInterval(id);
};
