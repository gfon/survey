import { createContext } from 'preact';

const SiteDetails = createContext({
  title: 'Litter Survey'
});

export default SiteDetails;
