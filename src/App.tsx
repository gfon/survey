import { h, Component, createRef } from 'preact';
import { useState } from 'preact/hooks';
import { Text } from 'preact-i18n';

import { FromStore } from './store';

import Welcome from './components/welcome';
import AddLitter from './components/add-litter';
import LitterList from './components/litter-list';
import LitterSurvey from './components/litter-survey';

const App = ({ user, picks, userSetWelcomed, settingsSetPermission }) => {
  const [show, setShow] = useState('add');
  const camera = createRef();

  const onAgree = (agree: boolean) => {
    console.log('got agree', agree, user);
    userSetWelcomed();
    settingsSetPermission('serviceWorker', agree);
  };

  return !user.welcomed ? (
    <main>
      <Welcome onAgree={onAgree} />
    </main>
  ) : (
    <main>
      <nav class="tabs">
        <ul role="tablist">
          <li
            role="presentation"
            class="litterPick"
            aria-selected={show === 'add' ? true : null}
          >
            <a role="tab" onClick={() => setShow('add')}>
              <Text id="app.tab.litter">Add litter</Text>
            </a>
          </li>
          <li
            role="presentation"
            aria-selected={show === 'litter' ? true : null}
          >
            <a role="tab" onClick={() => setShow('litter')}>
              <Text id="app.tab.showLitter">Litter</Text>({picks.litter.length})
            </a>
          </li>
          <li role="presentation" aria-selected={show === 'pick' ? true : null}>
            <a role="tab" onClick={() => setShow('pick')}>
              {picks.survey ? (
                <Text id="app.tab.showSurvey">Show litter survey</Text>
              ) : (
                <Text id="app.tab.startSurvey">Start litter survey</Text>
              )}
            </a>
          </li>
        </ul>
      </nav>
      {show === 'add' ? (
        <AddLitter />
      ) : show === 'litter' ? (
        <LitterList />
      ) : show === 'pick' ? (
        <LitterSurvey />
      ) : null}
    </main>
  );
};

export default FromStore(['user', 'picks'])(App);
