import { openDB } from 'idb';
import {
  nonSurveyLitterStore,
  storeLitterImage,
  storeLitter,
  storeSurvey
} from '../../storage';
import mcsIdMappings from '../../../taxon/mcsObjectMappings';
import {
  map as mcsObjects,
  mcsUrl,
  makeAttributes
} from '../../../taxon/mcsObjects';

const convertPosition = (position) => {
  const newPosition = {
    latitude: position.latitude,
    longitude: position.longitude
  };
  if (position.accuracy) {
    newPosition.accuracy = position.accuracy;
  }

  if (position.time || position.age) {
    newPosition.time = new Date(position.time).toISOString();
  }

  if (position.speed) {
    newPosition.speed = position.speed;
  }

  if (position.heading) {
    newPosition.heading = position.heading;
  }

  return newPosition;
};

const upgradeDb = (db, oldVersion, newVersion, transaction) => {
  db.createObjectStore('images');
  db.createObjectStore('surveys', { keyPath: 'id' });
  const litterStore = db.createObjectStore('litter', { keyPath: 'id' });

  litterStore.createIndex('survey', 'survey', { unique: false });
};

export const isCurrentVersion = () =>
  new Promise((resolve, reject) => {
    if (!window.indexedDB) {
      return new Error('No indexedDB suppot');
    }

    const request = window.indexedDB.open('db');

    request.onupgradeneeded = (event) => {
      resolve(Boolean(event.target.result.oldversion));
      event.target.transaction.abort();
    };

    request.onerror = () => {
      resolve(null);
    };
  });

export default async () => {
  const instance = await openDB('littersurvey', 1, {
    upgrade: upgradeDb
  });

  // Migrate everything to remoteStorage
  const transaction = instance.transaction(['litter', 'surveys', 'images']);
  const [litter, surveys] = await Promise.all([
    transaction.objectStore('litter').getAll(),
    transaction.objectStore('surveys').getAll()
  ]);

  const surveyMap = {};
  const nonSurveyLitter = [];

  if (litter.length) {
    for (let i = 0; i < litter.length; i++) {
      const litterItem = litter[i];

      const newLitterItem = {
        time: litterItem.time,
        observed: []
      };

      if (litterItem.mcsId && mcsIdMappings[litterItem.mcsId]) {
        const id = mcsIdMappings[litterItem.mcsId];
        const object = mcsObjects[id];
        const observed = {
          '@id': mcsUrl + mcsIdMappings[litterItem.mcsId],
          ...(makeAttributes(object) || {})
        };
        newLitterItem.observed.push(observed);
      }

      if (litterItem.position) {
        newLitterItem.location = [convertPosition(litterItem.position)];
      }

      if (litterItem.photos && litterItem.photos.length) {
        newLitterItem.media = await Promise.all(
          litterItem.photos.map((id) =>
            instance
              .transaction('images')
              .objectStore('images')
              .get(id)
              .then((blob) =>
                storeLitterImage(blob).then((id) => ({
                  '@id': id,
                  mimetype: blob.type
                }))
              )
          )
        );
      }

      if (litterItem.survey) {
        if (!surveyMap[litterItem.survey]) {
          // Find the survey
          const survey = surveys.find((item) => item.id === litterItem.survey);

          if (survey) {
            const newSurveyItem = {
              startTime: survey.start,
              endTime: survey.end,
              observations: []
            };

            if (survey.track && survey.track.length) {
              newSurveyItem.observers = [
                {
                  location: [
                    {
                      type: 'Track',
                      points: survey.track.map((position) =>
                        convertPosition(position)
                      )
                    }
                  ]
                }
              ];
            }

            surveyMap[litterItem.survey] = await storeSurvey(newSurveyItem);
          }
        }
      }

      // Store litter item
      const storedItem = await storeLitter(newLitterItem);

      if (litterItem.survey && surveyMap[litterItem.survey]) {
        newLitterItem.survey = surveyMap[litterItem.survey]['@id'];
        surveyMap[litterItem.survey].observations.push(storedItem['@id']);
      } else {
        nonSurveyLitter.push(storedItem['@id']);
      }
    }

    // Save all the survey items with attached observations
    const keys = Object.keys(surveyMap);

    for (let i = 0; i < keys.length; i++) {
      await storeSurvey(surveyMap[keys[i]]);
    }

    // Store non-survey litter list
    await nonSurveyLitterStore(nonSurveyLitter);
  }
};
