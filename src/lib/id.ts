/**
 * Make a random UUIDv4 ID
 */
export const makeId = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    // eslint-disable-next-line one-var
    const r = (Math.random() * 16) | 0,
      v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });

export const baseId = (url: string) => {
  if (!url) {
    return null;
  }

  const index = url.lastIndexOf('/');

  if (index !== -1) {
    return url.slice(index + 1);
  }

  return url;
};
