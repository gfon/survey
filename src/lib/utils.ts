export const zeroTens = (value) => (value < 10 ? '0' + value : value);

export const datetimeFormat = (date: Date | string) => {
  if (!(date instanceof Date)) {
    date = new Date(date);
  }

  return `${date.getFullYear()}-${zeroTens(date.getMonth() + 1)}-${zeroTens(
    date.getDate()
  )}T${zeroTens(date.getHours())}:${zeroTens(date.getMinutes())}`;
};

export interface HierarchicalItem<T = HierarchicalItem> {
  id: string;
  items?: Array<T>;
}

/**
 * Create a flat map of the items
 *
 * @param items Items to create a flat map of
 *
 * @returns A map of the items using their id as the map key
 */
export const makeMap = <T extends HierarchicalItem>(items: Array<T>) => {
  const map: { [id: string]: T } = {};

  for (let i = 0; i < items.length; i++) {
    map[items[i].id] = items[i];

    if (items[i].items) {
      Object.assign(map, makeMap(items[i].items));
    }
  }

  return map;
};

/**
 * Filter a hierarchy using the given filter function
 *
 * @param items Hierarchical items to filter
 * @param filterFunction Function to check if an item should kept in the
 *   filtered list
 * @param transformFunction Function to transform items as they are filtered
 *
 * @returns A filtered hierarchical list
 */
export const filterHierarchy = <T extends HierarchicalItem, R = T>(
  items: Array<T>,
  filterFunction: (item: T) => boolean,
  transformFunction?: (item: T) => R
) => {
  const filtered: Array<R> = [];

  for (let i = 0; i < items.length; i++) {
    let item = items[i];
    if (items[i].items) {
      item = {
        ...item,
        items: filterHierarchy(item.items, filterFunction, transformFunction)
      };
    }
    if ((item.items && item.items.length) || filterFunction(items[i])) {
      filtered.push(transformFunction ? transformFunction(item) : item);
    }
  }

  return filtered;
};

export const siNumber = (number: number, decimals: number = 1) => {
  if (!number) {
    return number;
  }

  if (number > 1000000000) {
    return (number / 1000000000).toFixed(decimals) + 'G';
  } else if (number > 1000000) {
    return (number / 1000000).toFixed(decimals) + 'M';
  } else if (number > 1000) {
    return (number / 1000).toFixed(decimals) + 'k';
  }

  return number.toFixed(decimals);
};
