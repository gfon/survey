/**
 * Return the path to the file relative to the given directory. Paths are
 * assumed to be starting in the same folder and a starting / is ignored
 */
export const getRelativePath = (
  file: string,
  directory: string,
  separator: string = '/'
) => {
  const fileParts = file.split(separator).filter((part) => part);
  const directoryParts = directory.split(separator).filter((part) => part);

  // Remove similar path
  while (fileParts[0] === directoryParts[0]) {
    fileParts.shift();
    directoryParts.shift();
  }

  directoryParts.fill('..');

  return directoryParts.concat(fileParts).join(separator);
};

/**
 * Return the base path of the relativePath, resolved using the relativeTo
 * path
 */
export const getBasePath = (
  relativePath: string,
  relativeTo: string,
  separator: string = '/'
) => {
  const pathParts = relativePath.split(separator);
  const relativeParts = relativeTo.split(separator);

  while (pathParts[0] === '..' && relativeParts.length) {
    relativeParts.pop();
    pathParts.shift();
  }

  return relativeParts.concat(pathParts).join(separator);
};
