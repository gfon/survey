import { Coordinate } from '../typings/location';

interface Options {
  enableHighAccuracy?: boolean;
  maximumAge?: number;
  timeout?: number;
}

const haveGeolocation =
  typeof navigator !== 'undefined' && 'geolocation' in navigator;

const defaultOptions = {
  enableHighAccuracy: true,
  maximumAge: 10000,
  timeout: 10000
};

const makePosition = (position: Position) => ({
  time: new Date(position.timestamp).toISOString(),
  latitude: position.coords.latitude,
  longitude: position.coords.longitude,
  accuracy: position.coords.accuracy,
  heading: position.coords.heading || undefined,
  speed: position.coords.speed || undefined
});

export const toLatLng = (position: Coordinate) => [
  position.latitude,
  position.longitude
];

export const getCurrentPosition = (options: Options = {}) =>
  new Promise((resolve, reject) => {
    if (!haveGeolocation) {
      reject(new Error('No Geolocation available'));
      return;
    }

    navigator.geolocation.getCurrentPosition(
      (position) => {
        resolve(makePosition(position));
      },
      reject,
      {
        ...defaultOptions,
        ...options
      }
    );
  });

export const watchPosition = (callback, error, options: Options = {}) => {
  if (!haveGeolocation) {
    throw Error('No Geolocation available');
  }

  return navigator.geolocation.watchPosition(
    (position) => callback(makePosition(position)),
    error,
    { ...defaultOptions, ...options }
  );
};

export const stopWatch = (id) => {
  if (haveGeolocation) {
    navigator.geolocation.clearWatch(id);
  }
};

/**
 * Convert angle in degrees to an angle in radians
 */
export const deg2rad = (deg) => {
  return deg * (Math.PI / 180);
};

/**
 * Convert angle in radians to an angle in degrees
 */
export const rad2deg = (rad) => {
  return rad / (Math.PI / 180);
};

/**
 * Calcuate the distance between 2 positions using leafet's distanceTo
 * function
 *
 * @returns Distance between the 2 points in meters
 */
export const distance = (pos1: Coordinate, pos2: Coordinate): number => {
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(pos2.latitude - pos1.latitude); // deg2rad below
  const dLon = deg2rad(pos2.longitude - pos1.longitude);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(pos1.latitude)) *
      Math.cos(deg2rad(pos2.latitude)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return d * 1000; // Return in meters
};
