export const getSurveyTrack = (survey) => {
  if (survey?.location) {
    const index = survey.location.findIndex(
      (location) => location.type === 'track'
    );

    if (index !== -1) {
      return survey.location[index].track;
    }
  }

  return null;
};
