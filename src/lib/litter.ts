import { Litter } from '../typings/litter';

import { filterHierarchy } from './utils';
import { baseId } from './id';
import objects, { map, LitterObject } from '../taxon/mcsObjects';
import { getImage, litterGetForSurvey } from './storage';

interface CountedLitterObject {
  count: number;
  litter?: Array<any>;
  item: LitterObject;
  items?: Array<CountedLitterObject>;
}

export const makeLitterHierarchy = (litter: Array<Litter>) => {
  const groupedLitter = {};
  for (let i = 0; i < litter.length; i++) {
    if (litter[i].observed && litter[i].observed.length) {
      for (let j = 0; j < litter[i].observed.length; j++) {
        const id = baseId(litter[i].observed[j]['@id']);
        if (id) {
          if (groupedLitter[id]) {
            groupedLitter[id].litter.push(litter[i]);
            groupedLitter[id].count++;
          } else {
            groupedLitter[id] = {
              count: 1,
              litter: [litter[i]],
              item: map[id]
            };
          }
        }
      }
    }
  }

  return filterHierarchy<LitterObject, CountedLitterObject>(
    objects,
    (item) => groupedLitter[item['id']],
    (item) => ({
      id: item['id'],
      item,
      items: item.items && item.items.length ? item.items : null,
      litter: groupedLitter[item['id']]?.litter,
      count:
        (groupedLitter[item['id']]?.count || 0) +
        (item.items?.length
          ? item.items.reduce((acc, item) => acc + item.count, 0)
          : 0)
    })
  );
};

/**
 * Load images for the given litter array
 *
 * @param litter Litter to load the images for
 *
 * @returns Litter with the images loaded
 */
export const loadLitterImages = (litter: Array<Litter>) => {
  const promises = [];
  for (let l = 0; l < litter.length; l++) {
    let item = litter[l];
    let changed = false;
    // TODO Need to get if public from somewhere
    const isPublic = !item.private;

    if (!item.media || !item.media.length) {
      continue;
    }

    for (let p = 0; p < item.media.length; p++) {
      if (
        item.media[p].mimetype.startsWith('image/') &&
        typeof item.media[p].blob === 'undefined'
      ) {
        if (!promises.length) {
          litter = [...litter];
        }
        if (!changed) {
          changed = true;
          litter[l] = {
            ...item,
            media: [...item.media]
          };
          item = litter[l];
        }

        const mediaObject = {
          '@id': item.media[p]['@id'],
          mimetype: item.media[p].mimetype,
          blob: null,
          url: null
        };
        item.media[p] = mediaObject;
        promises.push(
          getImage(mediaObject['@id'], 'litter', isPublic).then((image) => {
            if (image) {
              mediaObject.blob = image;
              mediaObject.url = URL.createObjectURL(image);
            }
          })
        );
      }
    }
  }

  if (promises.length) {
    return Promise.all(promises).then(() => litter);
  }

  return Promise.resolve(litter);
};

export const getLitter = (id?: string, ignore?: Array<string>) => {
  return litterGetForSurvey(id).then((litter) =>
    loadLitterImages(
      ignore ? litter.filter((item) => ignore.indexOf(item.id) === -1) : litter
    )
  );
};

export const getLitterLocation = (litter: Litter) => litter?.location?.[0];
