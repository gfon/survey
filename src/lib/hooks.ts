import { useState, useRef, useEffect, useLayoutEffect } from 'preact/hooks';

interface Reference<T> {
  value: T;
  set: (T) => void;
}

/**
 * State hook that includes a reference that can be used for async functions
 * It returns the same array as useState with an additional reference object
 * whose properties value and set will always be set to the latest value and
 * set function respectively
 *
 * @param initial Initial value for the state
 *
 * @returns [ value, setValue, referenceObect ]
 */
export const useRefState = <T>(initial: T): [T, (T) => void, Reference<T>] => {
  const [value, setValue] = useState(initial);
  const ref: Reference<T> = useRef(value) as any;

  ref.value = value;
  ref.set = setValue;

  useEffect(() => {
    ref.value = value;
    ref.set = setValue;
  }, [value, setValue]);

  return [value, setValue, ref];
};

/**
 * Use value from a CSS variable
 *
 * @param name Name of variable (without leading --)
 * @param defaultValue Default value for variable
 *
 * @returns Current variable value
 */
export const useCssVar = (
  nameMap: { [key: string]: string },
  defaultValue: any = null
) => {
  const ref = useRef({});

  useLayoutEffect(() => {
    const style = getComputedStyle(document.documentElement);

    ref.current = {};

    let newValue = null;

    const entries = Object.entries(nameMap);
    for (let i = 0; i < entries.length; i++) {
      const currentValue = style.getPropertyValue('--' + entries[i][1]) || null;

      if (currentValue !== ref.current[entries[i][0]]) {
        ref.current[entries[i][0]] = currentValue;
      }
    }
  });

  return ref;
};

/**
 * Use a ref containing a current prop value
 *
 * @param value Current prop value
 *
 * @returns A ref that will always point to the current prop value
 */
export const usePropRef = (value: any) => {
  const ref = useRef(value);

  useEffect(() => {
    ref.current = value;
  }, [value]);

  return ref;
};
