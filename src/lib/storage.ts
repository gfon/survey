import { makeId } from './id';
import createLitterDataModule from '../dataModules/index';
//TODO import { isCurrentVersion as indexedDbCurrent } from './storage/migrations/indexedDb';
import { getBasePath } from './path';

export const remoteStoreScope = 'litter';
export let litterStorage = null;
export let remoteStorage = import('remotestoragejs').then((RemoteStorage) => {
  return new Promise((resolve, reject) => {
    const rs = new RemoteStorage.default({
      modules: [createLitterDataModule(remoteStoreScope)]
    });
    rs.access.claim(remoteStoreScope, 'rw');
    let loaded = false;
    const ready = () => {
      if (loaded) return;
      loaded = true;
      remoteStorage = rs;
      litterStorage = remoteStorage[remoteStoreScope];
      resolve(remoteStorage);
    };
    rs.on('ready', ready);
  });
});

interface RemoteStorageParsedUrl {
  /// Whether or not the URL is public
  isPublic: boolean;
  /// The scope (root folder) of the URL
  scope: string;
  /// Whether or not the URL is a folder
  isDirectory: boolean;
  /// The data module the URL is from
  module: string;
  /// The
}

/**
 * Parse remoteStorage url
 *
 * @param url RemoteStorage URL to parse
 */
const _parseUrl = (
  url: string,
  relativeTo?: string,
  isPublic?: boolean
): RemoteStorageParsedUrl | string => {
  const result = {
    isPublic: isPublic || false,
    isDirectory: url.endsWith('/'),
    isRelative: false,
    scope: null,
    module: null,
    modulePath: null,
    path: null,
    fullPath: null,
    folderPath: null,
    id: null
  };

  if (
    remoteStorage.remote.connected &&
    url.startsWith(remoteStorage.remote.href)
  ) {
    url = url.slice(remoteStorage.remote.href.length);
  } else if (url.match(/^(https?|ftp):\/\//)) {
    return url;
  } else if (relativeTo) {
    url = getBasePath(url, relativeTo);
    result.isRelative = true;
    result.scope = remoteStoreScope;
  }

  if (url.startsWith('/')) {
    url = url.slice(1);
  }

  if (url.endsWith('/')) {
    url = url.slice(0, url.length - 1);
  }

  const parts = url.split('/');

  result.fullPath = parts.join('/') + (result.isDirectory ? '/' : '');
  if (parts[0] === 'public') {
    result.isPublic = true;
    parts.shift();
  }

  if (!result.isRelative) {
    result.scope = parts.shift();
    result.scopePath = parts.join('/') + (result.isDirectory ? '/' : '');
  }
  result.module = parts.shift();
  result.modulePath = parts.join('/') + (result.isDirectory ? '/' : '');
  result.id = parts.pop();
  if (parts.length) {
    result.path = parts.join('/');
  }

  return result;
};

/**
 * Parse remoteStorage url
 *
 * @param url RemoteStorage URL to parse
 */
export const parseUrl = (
  url: string
): Promise<RemoteStorageParsedUrl> | RemoteStorageParsedUrl => {
  if (remoteStorage instanceof Promise) {
    return remoteStorage.then(() => _parseUrl(url));
  }

  return _parseUrl(url);
};

let initialised = false;
export const initialise = async () => {
  if (initialised) {
    return;
  }

  initialised = true;

  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  /**TODO if (indexedDbCurrent()) {
    await import('./storage/migrations/indexedDb').then(
      (module) => module.default()
    );
  }*/
};

const _storeImage = async (
  image: Blob | File,
  store: string,
  relativeTo?: string
) => {
  const id = (store ? store + '/' : '') + makeId();

  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  return await litterStorage.images.store(id, image, true, relativeTo);
};

export const storeLitterImage = (image: Blob | File) =>
  _storeImage(image, 'litter', 'litter');

export const storeSurveyImage = (image: Blob | File) =>
  _storeImage(image, 'survey', 'survey');

export const getImage = async (
  id: string,
  relativeTo?: string,
  isPublic?: boolean
) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  if (id) {
    const parsed = _parseUrl(id, relativeTo, isPublic);

    if (parsed === id) {
      // TODO Fetch the url
      throw new Error('TODO');
    }

    if (parsed && parsed.scope === remoteStoreScope) {
      return await litterStorage.images.get(parsed.modulePath, parsed.isPublic);
    }

    return;
  }
};

export const nonSurveyLitterStore = async (nonSurveyLitter: Array<string>) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  return await litterStorage.nonSurveyLitter.store({
    litter: nonSurveyLitter
  });
};

export const nonSurveyLitterAdd = async (id: string) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  const nonSurveyLitter = (await litterStorage.nonSurveyLitter.get()) || {
    litter: []
  };

  nonSurveyLitter.litter.push(id);

  return await litterStorage.nonSurveyLitter.store(nonSurveyLitter);
};

export const nonSurveyLitterGet = async () => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  return await litterStorage.nonSurveyLitter.get();
};

export const storeSurvey = async (item) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  return await litterStorage.surveys.store(item);
};

export const surveyGet = async (id?: string) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  if (id) {
    const parsed = _parseUrl(id);

    if (parsed && parsed.scope === remoteStoreScope) {
      return await litterStorage.surveys.get(parsed.id, parsed.isPublic);
    }

    return;
  }

  return await litterStorage.surveys.getAll();
};

export const litterCount = async () => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  return await litterStorage.litter
    .listAll()
    .then((litter) => Object.keys(litter).length);
};

export const litterGetForSurvey = async (id?: string) => {
  let litterList = null;
  if (id) {
    const survey = await surveyGet(id);

    if (survey?.observations?.length) {
      litterList = survey.observations;
    }
  } else {
    const list = await nonSurveyLitterGet();

    if (list?.litter?.length) {
      litterList = list.litter;
    }
  }

  if (litterList?.length) {
    const promises = [];

    for (let i = 0; i < litterList.length; i++) {
      const url = litterList[i];
      const parsed = _parseUrl(url);

      if (parsed && parsed.scope === remoteStoreScope) {
        promises.push(litterStorage.litter.get(parsed.id, parsed.isPublic));
      }
    }

    return await Promise.all(promises).then((litter) => {
      const imagePromises = [];
      for (let i = 0; i < litter.length; i++) {
        if (litter[i].media?.length) {
          let j = 0;
          while (j < litter[i].media.length) {
            if (litter[i].media[j]?.mimetype?.startsWith('image/')) {
              const k = j;
              imagePromises.push(
                getImage(litter[i].media[j]['@id']).then((file) => {
                  if (!file) {
                    return;
                  }
                  litter[i].media[k].file = file;
                  litter[i].media[k].url = URL.createObjectURL(file);
                })
              );
              j++;
            } else {
              litter[i].media.splice(j, 1);
            }
          }
        }
      }

      return Promise.all(imagePromises).then(() => litter);
    });
  }

  return [];
};

export const storeLitter = async (item) => {
  if (remoteStorage instanceof Promise) {
    await remoteStorage;
  }

  let storeItem = item;

  const promises = [];

  // Clean photos
  if (item.media?.length) {
    storeItem = {
      ...item,
      media: [...item.media]
    };

    for (let i = 0; i < item.media.length; i++) {
      if (
        item.media[i].mimetype?.startsWith('image/') &&
        typeof item.media[i]['@id'] === 'undefined' &&
        item.media[i].blob
      ) {
        promises.push(
          storeLitterImage(item.media[i].blob).then((id) => {
            item.media[i]['@id'] = id;
            storeItem.media[i] = {
              '@id': id,
              mimetype: item.media[i].mimetype
            };
          })
        );
      }
    }
  }

  if (promises.length) {
    await Promise.all(promises);
  }

  return await litterStorage.litter.store(storeItem);

  if (item.photos?.length) {
    stores.push('images');
  }

  if (instance instanceof Promise) {
    await instance;
  }

  const transaction = instance.transaction(stores, 'readwrite');

  const toStore = {
    ...item,
    id,
    survey: item.survey || '',
    photos: item.photos.slice()
  };

  if (item.photos?.length) {
    for (let i = 0; i < item.photos.length; i++) {
      toStore.photos[i] = await _storeImage(item.photos[i].blob, transaction);
    }
  }

  if (toStore.time) {
    toStore.time = toStore.time.toISOString();
  }

  await transaction.objectStore('litter').put(toStore);

  await transaction.done;

  return toStore;
};

initialise();
