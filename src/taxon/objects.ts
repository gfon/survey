export default [
  {
    title: 'Fishing Gear'
    items: [
      {
        title: 'Fishboxes'
      },
      {
        title: 'Fishing line (angling)'
      },
      {
        title: 'Fishing net & net pieces: 0-50 cm'
      },
      {
        title: 'Fishing net & net pieces: 50 cm +'
      },
      {
        title: 'Lobster & fish tags'
      },
      {
        title: 'Lobster / crab pots & tops'
      },
      {
        title: 'Octopus pots'
      },
      {
        title: 'Oil containers / drums: 0-50 cm'
      },
      {
        title: 'Oil containers / drums: 50 cm +'
      },
      {
        title: 'Oyster nets / mussel bags (inc. plastic stoppers)'
      },
      {
        title: 'Oyster trays (round from oyster cultures)'
      },
      {
        title: 'Tangled nets / cord / rope / string'
      }
        Crab / lobster pots & tops
        Fish boxes
        Octopus pots
        Fishing weights / hooks / lures
        Sheeting from mussel culture (Tahitians)
        Lobster / crab pots & tops
        title: 'Floats / Buoys
    ]
  },
  {
    title: 'Clothing',
    items: [
        Gloves (e.g. washing up)
        Gloves (industrial/professional)
        Shoes / sandals
        Shoes (leather)
        Boots
        Clothing / shoes / towels
    ]
  },
  {
    title: 'Household Items',
    items: [
      {
        title: 'Bottles / containers: cleaner'
      },
      {
        title: 'Bottles / containers: toiletries / cosmetics'
      },
      {
        title: 'Buckets'
      },
        Light / glow sticks (tubes with fluid)
        Furnishings
        Appliances
        BBQs (disposable)
        Toys / party poppers / fireworks / dummies
    ]
  },
  {
    title: 'Tabacco / Vaping Items',
    items: [
      {
        title: 'Cigarette lighters'
      },
      {
        title: ' / tobacco pouches'
      },
        Cigarette packets
        Cigarette stubs
    ]
  },
  {
    title: 'Personal Items / Health',
    items: [
      {
        title: 'Combs / hair brushes / sunglasses'
      },
        Condoms
        Cotton bud sticks
        Tampons & applicators
        Toilet fresheners
        Towels / panty liners / backing strips
        Wet wipes
        Containers / tubes (inc. pill packets)
        Syringes & needles
    ]
  },
  {
    title: 'Food/Drink',
    items: [
      {
        title: 'Cups'
      },
      {
        title: 'Cutlery / trays / straws'
      },
      {
        title: '4/6 pack rings/yokes'
      },
      {
        title: 'Shopping bags'
      },
      {
        title: 'Bag ends'
        'Bags (e.g. shopping)'
        'Bags: Mesh (e.g. vegetable)'
        'Bags: Small (e.g. freezer / vegetable)'
      },
      {
        title: 'Bottles / containers / drums: Other'
      },
      {
        title: 'Bottles / containers: drinks'
      },
      {
        title: 'Caps / lids'
      },
      {
        title: 'Containers: Food (inc. fast food)'
      },
        Packets: Crisp / sweet / lolly (inc sticks) / sandwich
        Lolly sticks / chip forks
        Cans (drink)
        Cans (food)
        Caps / lids
        Foil wrappers

      {
        title: 'Car parts'
      },
      {
        title: 'Crates'
      },
      {
        title: 'Fertiliser / animal feed bags'
      },
      {
        title: 'Fibreglass'
      },
      {
        Foam / sponge / insulation
        Hard hats
        Injection gun cartridge (e.g. sealant)
        Jerry cans
        Packaging / plastic sheeting (industrial)
        Pens & pen lids
        Plastic / polystyrene pieces: 0 - 2.5 cm
        Plastic / polystyrene pieces: 2.5 - 50 cm
        Plastic / polystyrene pieces: 50 cm +
        Shotgun cartridges
        Strapping bands
        String / cord / rope: thickness 0-1 cm
        String / cord / rope: thickness 1 cm +
        Other (please specify
        Balloons (inc string, valves, ribbons)
        Tyres & engine belts
        Tyres used as fenders
        Sacking
        Bags
        Cardboard
        Cartons (purepak e.g. milk)
        Cartons (tetrapak e.g. juice)
        Cups
        Newspapers / magazines
        Corks
        Crates
        Paint brushes
        Pallets
        Other 0-50 cm (please specify)
        Other 50+ cm

        Aerosol / spray cans
        Oil drums
        Paint tins
        Scrap
        Wire / mesh / barbed wire
        Other 0-50 cm (please specify)
        Other 50+ cm (please

        Bottles
        Light bulbs / tubes
        Other (please specify

        Construction material (e.g. tiles)

        Paraffin / wax pieces: 0-1cm
        Paraffin / wax pieces: 1-10cm
        Paraffin / wax pieces: 10cm
]
