import { makeMap, HierarchicalItem } from '../lib/utils';
import { materialUrl } from './materials';
import { typeUrl } from './type';
import { sizeUrl } from './size';

export interface LitterObject extends HierarchicalItem<LitterObject> {
  title: string;
  label?: string;
  selectable?: boolean;
  type?: string;
  material?: string | Array<string>;
  size?: 'small' | 'medium' | 'large' | [number, number, number];
  brand?: string;
}

export const mcsUrl = 'https://vocab.gfon.org/litter/mcs-ml/v0/';

export const makeAttributes = (item: LitterObject) => {
  const attributes = {};
  let haveAttributes = false;

  if (!item) {
    return null;
  }

  if (item.size) {
    attributes.size = { '@id': item.size };
    haveAttributes = true;
  }
  if (item.material) {
    attributes.material = { '@id': item.material };
    haveAttributes = true;
  }
  if (haveAttributes) {
    return attributes;
  }

  return null;
};

const objects: Array<LitterObject> = [
  {
    id: '1',
    title: 'Plastic / Polystyrene',
    selectable: false,
    items: [
      {
        id: '101',
        title: '4/6 pack yokes',
        type: typeUrl + '1',
        material: materialUrl + '8'
      },
      {
        id: '102',
        title: 'Bag ends',
        type: typeUrl + '2',
        material: materialUrl + '8'
      },
      {
        id: '103',
        title: 'Plastic bags (e.g. shopping)',
        label: 'Bags (e.g. shopping)',
        type: typeUrl + '3',
        material: materialUrl + '8'
      },
      {
        id: '104',
        title: 'Plastic bags: mesh (e.g. vegetable)',
        label: 'Bags: mesh (e.g. vegetable)',
        type: typeUrl + '4',
        material: materialUrl + '8'
      },
      {
        id: '105',
        title: 'Plastic bags: small (e.g. freezer / vegetable)',
        label: 'Bags: small (e.g. freezer / vegetable)',
        type: typeUrl + '5',
        material: materialUrl + '8'
      },
      {
        id: '106',
        title: 'Plastic bottles / containers / drums: Other',
        label: 'Bottles / containers / drums: Other',
        type: typeUrl + '6',
        material: materialUrl + '8'
      },
      {
        id: '107',
        title: 'Plastic bottles / containers: cleaner',
        label: 'Bottles / containers: cleaner',
        type: typeUrl + '7',
        material: materialUrl + '8'
      },
      {
        id: '108',
        title: 'Plastic bottles / containers: drinks',
        label: 'Bottles / containers: drinks',
        type: typeUrl + '8',
        material: materialUrl + '8'
      },
      {
        id: '109',
        title: 'Plastic bottles / containers: toiletries / cosmetics',
        label: 'Bottles / containers: toiletries / cosmetics',
        type: typeUrl + '9',
        material: materialUrl + '8'
      },
      {
        id: '110',
        title: 'Plastic buckets',
        type: typeUrl + '10',
        material: materialUrl + '8'
      },
      {
        id: '111',
        title: 'Plastic caps / lids',
        label: 'Caps / lids',
        type: typeUrl + '11',
        material: materialUrl + '8'
      },
      {
        id: '112',
        title: 'Plastic car parts',
        label: 'Car parts',
        type: typeUrl + '12',
        material: materialUrl + '8'
      },
      {
        id: '113',
        title: 'Cigarette lighters / tobacco pouches',
        type: typeUrl + '13',
        material: materialUrl + '8'
      },
      {
        id: '114',
        title: 'Combs / hair brushes / sunglasses',
        type: typeUrl + '14',
        material: materialUrl + '8'
      },
      {
        id: '115',
        title: 'Plastic containers: Food (inc. fast food)',
        label: 'Containers: Food (inc. fast food)',
        type: typeUrl + '15',
        material: materialUrl + '8'
      },
      {
        id: '116',
        title: 'Crates',
        type: typeUrl + '16',
        material: materialUrl + '8'
      },
      {
        id: '117',
        title: 'Plastic cup',
        label: 'Cup',
        type: typeUrl + '17',
        material: materialUrl + '8'
      },
      {
        id: '118',
        title: 'Cutlery / trays / straws',
        type: typeUrl + '18',
        material: materialUrl + '8'
      },
      {
        id: '119',
        title: 'Fertiliser / animal feed bags',
        type: typeUrl + '19',
        material: materialUrl + '8'
      },
      {
        id: '120',
        title: 'Fibreglass',
        type: typeUrl + '20',
        material: materialUrl + '8'
      },
      {
        id: '121',
        title: 'Fishboxes',
        type: typeUrl + '21',
        material: materialUrl + '8'
      },
      {
        id: '122',
        title: 'Fishing line (angling)',
        type: typeUrl + '22',
        material: materialUrl + '8'
      },
      {
        id: '123',
        title: 'Fishing net & net pieces: 0-50 cm',
        type: typeUrl + '23',
        size: sizeUrl + '500',
        material: materialUrl + '8'
      },
      {
        id: '124',
        title: 'Fishing net & net pieces: 50 cm +',
        type: typeUrl + '23',
        size: sizeUrl + '501',
        material: materialUrl + '8'
      },
      {
        id: '125',
        title: 'Floats / buoys',
        type: typeUrl + '24',
        material: materialUrl + '8'
      },
      {
        id: '126',
        title: 'Foam / sponge / insulation',
        type: typeUrl + '25',
        material: materialUrl + '8'
      },
      {
        id: '127',
        title: 'Gloves (e.g. washing up)',
        type: typeUrl + '26',
        material: materialUrl + '8'
      },
      {
        id: '128',
        title: 'Gloves (industrial/professional)',
        type: typeUrl + '27',
        material: materialUrl + '8'
      },
      {
        id: '129',
        title: 'Hard hats',
        type: typeUrl + '28',
        material: materialUrl + '8'
      },
      {
        id: '130',
        title: 'Injection gun cartridge (e.g. sealant)',
        type: typeUrl + '29',
        material: materialUrl + '8'
      },
      {
        id: '131',
        title: 'Plastic jerry cans',
        label: 'Jerry cans',
        type: typeUrl + '30',
        material: materialUrl + '8'
      },
      {
        id: '132',
        title: 'Light / glow sticks (tubes with fluid)',
        type: typeUrl + '31',
        material: materialUrl + '8'
      },
      {
        id: '133',
        title: 'Plastic lobster & fish tags',
        label: 'Lobster & fish tags',
        type: typeUrl + '32',
        material: materialUrl + '8'
      },
      {
        id: '134',
        title: 'Plastic lobster / crab pots & tops',
        label: 'Lobster / crab pots & tops',
        type: typeUrl + '33',
        material: materialUrl + '8'
      },
      {
        id: '135',
        title: 'Plastic octopus pots',
        label: 'Octopus pots',
        type: typeUrl + '34',
        material: materialUrl + '8'
      },
      {
        id: '136',
        title: 'Plastic oil containers / drums: 0-50 cm',
        label: 'Oil containers / drums: 0-50 cm',
        type: typeUrl + '35',
        size: sizeUrl + '500',
        material: materialUrl + '8'
      },
      {
        id: '137',
        title: 'Plastic oil containers / drums: 50 cm +',
        label: 'Oil containers / drums: 50 cm +',
        type: typeUrl + '35',
        size: sizeUrl + '501',
        material: materialUrl + '8'
      },
      {
        id: '138',
        title: 'Oyster nets / mussel bags (inc. plastic stoppers)',
        type: typeUrl + '36',
        material: materialUrl + '8'
      },
      {
        id: '139',
        title: 'Plastic oyster trays (round from oyster cultures)',
        label: 'Oyster trays (round from oyster cultures)',
        type: typeUrl + '37',
        material: materialUrl + '8'
      },
      {
        id: '140',
        title: 'Packaging / plastic sheeting (industrial)',
        type: typeUrl + '38',
        material: materialUrl + '8'
      },
      {
        id: '141',
        title: 'Packets: Crisp / sweet / lolly (inc sticks) / sandwich',
        type: typeUrl + '39',
        material: materialUrl + '8'
      },
      {
        id: '142',
        title: 'Pens & pen lids',
        type: typeUrl + '40',
        material: materialUrl + '8'
      },
      {
        id: '143',
        title: 'Plastic / polystyrene pieces: 0 - 2.5 cm',
        type: typeUrl + '41',
        size: sizeUrl + '25',
        material: materialUrl + '8'
      },
      {
        id: '144',
        title: 'Plastic / polystyrene pieces: 2.5 - 50 cm',
        type: typeUrl + '41',
        size: sizeUrl + '500',
        material: materialUrl + '8'
      },
      {
        id: '145',
        title: 'Plastic / polystyrene pieces: 50 cm +',
        type: typeUrl + '41',
        size: sizeUrl + '501',
        material: materialUrl + '8'
      },
      {
        id: '146',
        title: 'Sheeting from mussel culture (Tahitians)',
        type: typeUrl + '42',
        material: materialUrl + '8'
      },
      {
        id: '147',
        title: 'Plastic shoes / sandals',
        label: 'Shoes / sandals',
        type: typeUrl + '43',
        material: materialUrl + '8'
      },
      {
        id: '148',
        title: 'Shotgun cartridges',
        type: typeUrl + '44',
        material: materialUrl + '8'
      },
      {
        id: '149',
        title: 'Strapping bands',
        type: typeUrl + '45',
        material: materialUrl + '8'
      },
      {
        id: '150',
        title: 'String / cord / rope: thickness 0-1 cm',
        type: typeUrl + '46',
        material: materialUrl + '8'
      },
      {
        id: '151',
        title: 'String / cord / rope: thickness 1 cm +',
        type: typeUrl + '47',
        material: materialUrl + '8'
      },
      {
        id: '152',
        title: 'Tangled nets / cord / rope / string',
        type: typeUrl + '48',
        material: materialUrl + '8'
      },
      {
        id: '153',
        title: 'Toys / party poppers / fireworks / dummies',
        type: typeUrl + '49',
        material: materialUrl + '8'
      },
      {
        id: '154',
        title: 'Other plastic',
        label: 'Other',
        material: materialUrl + '8'
      }
    ]
  },
  {
    id: '2',
    title: 'Rubber',
    selectable: false,
    items: [
      {
        id: '201',
        title: 'Balloons (inc string, valves, ribbons)',
        type: typeUrl + '50',
        material: materialUrl + '10'
      },
      {
        id: '202',
        title: 'Rubber boots',
        type: typeUrl + '51',
        material: materialUrl + '10'
      },
      {
        id: '203',
        title: 'Tyres & engine belts',
        type: typeUrl + '12',
        material: materialUrl + '10'
      },
      {
        id: '204',
        title: 'Tyres used as fenders',
        type: typeUrl + '52',
        material: materialUrl + '10'
      },
      {
        id: '205',
        title: 'Other rubber',
        label: 'Other',
        material: materialUrl + '10'
      }
    ]
  },
  {
    id: '3',
    title: 'Cloth',
    selectable: false,
    items: [
      {
        id: '301',
        title: 'Clothing / shoes / towels',
        type: typeUrl + '53',
        material: materialUrl + '12'
      },
      {
        id: '302',
        title: 'Furnishings',
        type: typeUrl + '54',
        material: materialUrl + '12'
      },
      {
        id: '303',
        title: 'Sacking',
        type: typeUrl + '55',
        material: materialUrl + '12'
      },
      {
        id: '304',
        title: 'Shoes (leather)',
        type: typeUrl + '43',
        material: materialUrl + '12'
      },
      {
        id: '305',
        title: 'Other label',
        label: 'Other',
        material: materialUrl + '12'
      }
    ]
  },
  {
    id: '4',
    title: 'Paper / Cardboard',
    selectable: false,
    items: [
      {
        id: '401',
        title: 'Paper bags',
        label: 'Bags',
        type: typeUrl + '3',
        material: materialUrl + '18'
      },
      {
        id: '402',
        title: 'Cardboard',
        type: typeUrl + '56',
        material: materialUrl + '18'
      },
      {
        id: '403',
        title: 'Cartons (purepak e.g. milk)',
        type: typeUrl + '57',
        material: materialUrl + '18'
      },
      {
        id: '404',
        title: 'Cartons (tetrapak e.g. juice)',
        type: typeUrl + '58',
        material: materialUrl + '18'
      },
      {
        id: '405',
        title: 'Cigarette packets',
        type: typeUrl + '59',
        material: materialUrl + '18'
      },
      {
        id: '406',
        title: 'Cigarette stubs',
        type: typeUrl + '60',
        material: materialUrl + '18'
      },
      {
        id: '407',
        title: 'Paper cups',
        label: 'Cups',
        type: typeUrl + '17',
        material: materialUrl + '18'
      },
      {
        id: '408',
        title: 'Newspapers / magazines',
        type: typeUrl + '61',
        material: materialUrl + '18'
      },
      {
        id: '409',
        title: 'Other paper / cardboard',
        label: 'Other',
        material: materialUrl + '18'
      }
    ]
  },
  {
    id: '5',
    title: 'Wood',
    selectable: false,
    items: [
      {
        id: '501',
        title: 'Corks',
        type: typeUrl + '62',
        material: materialUrl + '9'
      },
      {
        id: '502',
        title: 'Lolly sticks / chip forks',
        type: typeUrl + '63',
        material: materialUrl + '9'
      },
      {
        id: '503',
        title: 'Crab / lobster pots & tops',
        type: typeUrl + '33',
        material: materialUrl + '9'
      },
      {
        id: '504',
        title: 'Wood crates',
        label: 'Crates',
        type: typeUrl + '16',
        material: materialUrl + '9'
      },
      {
        id: '505',
        title: 'Wood fish boxes',
        label: 'Fish boxes',
        type: typeUrl + '64',
        material: materialUrl + '9'
      },
      {
        id: '506',
        title: 'Paint brushes',
        type: typeUrl + '65',
        material: materialUrl + '9'
      },
      {
        id: '507',
        title: 'Wood pallets',
        label: 'Pallets',
        type: typeUrl + '66',
        material: materialUrl + '9'
      },
      {
        id: '508',
        title: 'Other wood 0-50 cm (please specify)',
        label: 'Other 0-50 cm (please specify)',
        type: typeUrl + '41',
        material: materialUrl + '9',
        size: sizeUrl + '500'
      },
      {
        id: '509',
        title: 'Other wood 50+ cm (please specify)',
        label: 'Other 50+ cm (please specify)',
        type: typeUrl + '41',
        material: materialUrl + '9',
        size: sizeUrl + '501'
      }
    ]
  },
  {
    id: '6',
    title: 'Metal',
    selectable: false,
    items: [
      {
        id: '601',
        title: 'Aerosol / spray cans',
        type: typeUrl + '67',
        material: materialUrl + '15'
      },
      {
        id: '602',
        title: 'Appliances',
        type: typeUrl + '68',
        material: materialUrl + '15'
      },
      {
        id: '603',
        title: 'BBQs (disposable)',
        type: typeUrl + '69',
        material: materialUrl + '15'
      },
      {
        id: '604',
        title: 'Cans (drink)',
        type: typeUrl + '70',
        material: materialUrl + '15'
      },
      {
        id: '605',
        title: 'Cans (food)',
        type: typeUrl + '71',
        material: materialUrl + '15'
      },
      {
        id: '606',
        title: 'Metal caps / lids',
        label: 'Caps / lids',
        type: typeUrl + '11',
        material: materialUrl + '15'
      },
      {
        id: '607',
        title: 'Fishing weights / hooks / lures',
        type: typeUrl + '72',
        material: materialUrl + '15'
      },
      {
        id: '608',
        title: 'Foil wrappers',
        type: typeUrl + '73',
        material: materialUrl + '15'
      },
      {
        id: '609',
        title: 'Metal lobster / crab pots & tops',
        label: 'Lobster / crab pots & tops',
        type: typeUrl + '33',
        material: materialUrl + '15'
      },
      {
        id: '610',
        title: 'Metal oil drums',
        label: 'Oil drums',
        type: typeUrl + '74',
        material: materialUrl + '15'
      },
      {
        id: '611',
        title: 'Paint tins',
        type: typeUrl + '75',
        material: materialUrl + '15'
      },
      {
        id: '612',
        title: 'Metal scrap',
        label: 'Scrap',
        type: typeUrl + '41',
        material: materialUrl + '15'
      },
      {
        id: '613',
        title: 'Wire / mesh / barbed wire',
        type: typeUrl + '76',
        material: materialUrl + '15'
      },
      {
        id: '614',
        title: 'Other metal 0-50 cm (please specify)',
        label: 'Other 0-50 cm (please specify)',
        type: typeUrl + '41',
        size: sizeUrl + '500',
        material: materialUrl + '15'
      },
      {
        id: '615',
        title: 'Other metal 50+ cm (please specify)',
        label: 'Other 50+ cm (please specify)',
        type: typeUrl + '41',
        size: sizeUrl + '501',
        material: materialUrl + '15'
      }
    ]
  },
  {
    id: '7',
    title: 'Glass',
    selectable: false,
    items: [
      {
        id: '701',
        title: 'Bottles',
        type: typeUrl + '77',
        material: materialUrl + '11'
      },
      {
        id: '702',
        title: 'Light bulbs / tubes',
        type: typeUrl + '78',
        material: materialUrl + '11'
      },
      {
        id: '703',
        title: 'Other glass',
        label: 'Other',
        material: materialUrl + '11'
      }
    ]
  },
  {
    id: '8',
    title: 'Pottery / Ceramics',
    selectable: false,
    items: [
      {
        id: '801',
        title: 'Construction material (e.g. tiles)',
        type: typeUrl + '79',
        material: materialUrl + '19'
      },
      {
        id: '802',
        title: 'Octopus pots',
        type: typeUrl + '34',
        material: materialUrl + '19'
      },
      {
        id: '803',
        title: 'Other ceramics',
        label: 'Other',
        material: materialUrl + '19'
      }
    ]
  },
  {
    id: '9',
    title: 'Sanitary',
    selectable: false,
    items: [
      {
        id: '901',
        title: 'Condoms',
        type: typeUrl + '80'
      },
      {
        id: '902',
        title: 'Cotton bud sticks',
        type: typeUrl + '81'
      },
      {
        id: '903',
        title: 'Tampons & applicators',
        type: typeUrl + '82'
      },
      {
        id: '904',
        title: 'Toilet fresheners',
        type: typeUrl + '83'
      },
      {
        id: '904',
        title: 'Towels / panty liners / backing strips',
        type: typeUrl + '84'
      },
      {
        id: '905',
        title: 'Wet wipes',
        type: typeUrl + '85'
      },
      {
        id: '906',
        title: 'Other sanitary items',
        label: 'Other'
      }
    ]
  },
  {
    id: '10',
    title: 'Medical',
    selectable: false,
    items: [
      {
        id: '1001',
        title: 'Containers / tubes (inc. pill packets)',
        type: typeUrl + '86'
      },
      {
        id: '1002',
        title: 'Syringes & needles',
        type: typeUrl + '87'
      },
      {
        id: '1003',
        title: 'Face mask',
        type: typeUrl + '88'
      },
      {
        id: '1004',
        title: 'Medical Glove',
        type: typeUrl + '89',
        material: materialUrl + '10'
      },
      {
        id: '1005',
        title: 'Other medical items',
        label: 'Other'
      }
    ]
  },
  {
    id: '11',
    title: 'Faeces',
    selectable: false,
    items: [
      {
        id: '1101',
        title: 'Bagged dog faeces',
        type: typeUrl + '90'
      }
    ]
  },
  {
    id: '12',
    title: 'Pollutants',
    selectable: false,
    items: [
      {
        id: '1201',
        title: 'Paraffin / wax pieces: 0-1cm',
        type: typeUrl + '41',
        size: sizeUrl + '10',
        material: materialUrl + '20'
      },
      {
        id: '1202',
        title: 'Paraffin / wax pieces: 1-10cm',
        type: typeUrl + '41',
        size: sizeUrl + '100',
        material: materialUrl + '20'
      },
      {
        id: '1203',
        title: 'Paraffin / wax pieces: 10cm +',
        type: typeUrl + '41',
        size: sizeUrl + '101',
        material: materialUrl + '20'
      },
      {
        id: '1204',
        title: 'Other pollutants',
        label: 'Other'
      }
    ]
  }
];

export const map = makeMap(objects);

export default objects;
