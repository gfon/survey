export default [
  {
    title: 'Non-alcoholic Drinks',
    selectable: false,
    items: [
      {
        id: 'https://www.wikidata.org/wiki/Q3295867',
        title: 'Coca-Cola Company',
        selectable: false,
        items: [
          {
            id: 'https://www.wikidata.org/wiki/Q2813',
            title: 'Coca-Cola',
            otherNames: ['coke']
          }
        ]
      }
    ]
  }
];
