export const materialUrl = 'https://vocab.gfon.org/material/v0/';

export default [
  {
    id: '8',
    title: 'Plastic',
    items: [
      {
        id: '1',
        tag: 'plastic-1',
        label: '1 - PET',
        title: 'PET (Polyethylene Terephthalate)',
      },
      {
        id: '2',
        tag: 'plastic-2',
        label: '2 - HDPE',
        title: 'HDPE (High Density Polyethylene)',
      },
      {
        id: '3',
        tag: 'plastic-3',
        label: '3 - PVC',
        title: 'PVC (Vinyl)',
      },
      {
        id: '4',
        tag: 'plastic-4',
        label: '4 - LDPE',
        title: 'LDPE (Low Density Polyethylene)',
      },
      {
        id: '5',
        tag: 'plastic-5',
        label: '5 - PP',
        title: 'PP Polypropylene()',
      },
      {
        id: '6',
        tag: 'plastic-6',
        label: '6 - PS',
        title: 'PS (Polystyrene)',
      },
      {
        id: '7',
        tag: 'plastic-7',
        label: '7 - Other',
      }
    ]
  },
  {
    id: '9',
    title: 'Wood'
  },
  {
    id: '10',
    title: 'Rubber'
  },
  {
    id: '11',
    title: 'Glass'
  },
  {
    id: '12',
    title: 'Cloth',
    items: [
      {
        id: '13',
        title: 'Synthetic'
      },
      {
        id: '14',
        title: 'Natural'
      }
    ]
  },
  {
    id: '15',
    title: 'Metal',
    items: [
      {
        id: '16',
        title: 'Aluminium'
      },
      {
        id: '17',
        title: 'Steel'
      }
    ]
  },
  {
    id: '18',
    title: 'Paper/Card'
  },
  {
    id: '19',
    title: 'Pottery/Ceramics'
  },
  {
    id: '20',
    title: 'Wax'
  },
]
