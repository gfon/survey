export const typeUrl = 'https://vocab.gfon.org/litter/types/v0/';

export default [
  {
    id: 1,
    title: '4/6 pack yokes'
  },
  {
    id: 2,
    title: 'Bag ends'
  },
  {
    id: 3,
    title: 'Bags (e.g. shopping)'
  },
  {
    id: 4,
    title: 'Bags: mesh (e.g. vegetable)'
  },
  {
    id: 5,
    title: 'Bags: small (e.g. freezer / vegetable)'
  },
  {
    id: 6,
    title: 'Bottles / containers / drums: other'
  },
  {
    id: 7,
    title: 'Bottles / containers: cleaner'
  },
  {
    id: 8,
    title: 'Bottles / containers: drinks'
  },
  {
    id: 9,
    title: 'Bottles / containers: toiletries / cosmetics'
  },
  {
    id: 10,
    title: 'Buckets'
  },
  {
    id: 11,
    title: 'Caps / lids'
  },
  {
    id: 12,
    title: 'Car parts'
  },
  {
    id: 13,
    title: 'Cigarette lighters / tobacco pouches'
  },
  {
    id: 14,
    title: 'Combs / hair brushes / sunglasses'
  },
  {
    id: 15,
    title: 'Containers: Food (inc. fast food)'
  },
  {
    id: 16,
    title: 'Crates'
  },
  {
    id: 17,
    title: 'Cup'
  },
  {
    id: 18,
    title: 'Cutlery / trays / straws'
  },
  {
    id: 19,
    title: 'Fertiliser / animal feed bags'
  },
  {
    id: 20,
    title: 'Fibreglass'
  },
  {
    id: 21,
    title: 'Fishboxes'
  },
  {
    id: 22,
    title: 'Fishing line (angling)'
  },
  {
    id: 23,
    title: 'Fishing net & net pieces: 0-50 cm'
  },
  {
    id: 24,
    title: 'Floats / buoys'
  },
  {
    id: 25,
    title: 'Foam / sponge / insulation'
  },
  {
    id: 26,
    title: 'Gloves (e.g. washing up)'
  },
  {
    id: 27,
    title: 'Gloves (industrial/professional)'
  },
  {
    id: 28,
    title: 'Hard hats'
  },
  {
    id: 29,
    title: 'Injection gun cartridge (e.g. sealant)'
  },
  {
    id: 30,
    title: 'Jerry cans'
  },
  {
    id: 31,
    title: 'Light / glow sticks (tubes with fluid)'
  },
  {
    id: 32,
    title: 'Lobster & fish tags'
  },
  {
    id: 33,
    title: 'Lobster / crab pots & tops'
  },
  {
    id: 34,
    title: 'Octopus pots'
  },
  {
    id: 35,
    title: 'Oil containers / drums: 0-50 cm'
  },
  {
    id: 36,
    title: 'Oyster nets / mussel bags (inc. stoppers)'
  },
  {
    id: 37,
    title: 'Oyster trays (round from oyster cultures)'
  },
  {
    id: 38,
    title: 'Packaging / sheeting (industrial)'
  },
  {
    id: 39,
    title: 'Packets: Crisp / sweet / lolly (inc sticks) / sandwich'
  },
  {
    id: 40,
    title: 'Pens & pen lids'
  },
  {
    id: 41,
    title: 'Pieces / scrap'
  },
  {
    id: 42,
    title: 'Sheeting from mussel culture (Tahitians)'
  },
  {
    id: 43,
    title: 'Shoes / sandals'
  },
  {
    id: 44,
    title: 'Shotgun cartridges'
  },
  {
    id: 45,
    title: 'Strapping bands'
  },
  {
    id: 46,
    title: 'String / cord / rope: thickness 0-1 cm'
  },
  {
    id: 47,
    title: 'String / cord / rope: thickness 1 cm +'
  },
  {
    id: 48,
    title: 'Tangled nets / cord / rope / string'
  },
  {
    id: 49,
    title: 'Toys / party poppers / fireworks / dummies'
  },
  {
    id: 50,
    title: 'Balloons (inc string, valves, ribbons)'
  },
  {
    id: 51,
    title: 'Rubber boots'
  },
  {
    id: 52,
    title: 'Tyres used as fenders'
  },
  {
    id: 53,
    title: 'Clothing / shoes / towels'
  },
  {
    id: 54,
    title: 'Furnishings'
  },
  {
    id: 55,
    title: 'Sacking'
  },
  {
    id: 56,
    title: 'Cardboard'
  },
  {
    id: 57,
    title: 'Cartons (purepak e.g. milk)'
  },
  {
    id: 58,
    title: 'Cartons (tetrapak e.g. juice)'
  },
  {
    id: 59,
    title: 'Cigarette packets'
  },
  {
    id: 60,
    title: 'Cigarette stubs'
  },
  {
    id: 61,
    title: 'Newspapers / magazines'
  },
  {
    id: 62,
    title: 'Corks'
  },
  {
    id: 63,
    title: 'Lolly sticks / chip forks'
  },
  {
    id: 64,
    title: 'Wood fish boxes'
  },
  {
    id: 65,
    title: 'Paint brushes'
  },
  {
    id: 66,
    title: 'Wood pallets'
  },
  {
    id: 67,
    title: 'Aerosol / spray cans'
  },
  {
    id: 68,
    title: 'Appliances'
  },
  {
    id: 69,
    title: 'BBQs (disposable)'
  },
  {
    id: 70,
    title: 'Cans (drink)'
  },
  {
    id: 71,
    title: 'Cans (food)'
  },
  {
    id: 72,
    title: 'Fishing weights / hooks / lures'
  },
  {
    id: 73,
    title: 'Foil wrappers'
  },
  {
    id: 74,
    title: 'Metal oil drums'
  },
  {
    id: 75,
    title: 'Paint tins'
  },
  {
    id: 76,
    title: 'Wire / mesh / barbed wire'
  },
  {
    id: 77,
    title: 'Bottles'
  },
  {
    id: 78,
    title: 'Light bulbs / tubes'
  },
  {
    id: 79,
    title: 'Construction material (e.g. tiles)'
  },
  {
    id: 80,
    title: 'Condoms'
  },
  {
    id: 81,
    title: 'Cotton bud sticks'
  },
  {
    id: 82,
    title: 'Tampons & applicators'
  },
  {
    id: 83,
    title: 'Toilet fresheners'
  },
  {
    id: 84,
    title: 'Towels / panty liners / backing strips'
  },
  {
    id: 85,
    title: 'Wet wipes'
  },
  {
    id: 86,
    title: 'Containers / tubes (inc. pill packets)'
  },
  {
    id: 87,
    title: 'Syringes & needles'
  },
  {
    id: 88,
    title: 'Face mask'
  },
  {
    id: 89,
    title: 'Medical glove'
  },
  {
    id: 90,
    title: 'Bagged dog faeces'
  }
]
