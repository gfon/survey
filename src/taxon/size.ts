export const sizeUrl = 'https://vocab.gfon.org/litter/size/v0/';

export default [
  {
    id: 5,
    title: 'Smaller than 5mm'
  },
  {
    id: 10,
    title: 'Smaller than 10mm'
  },
  {
    id: 25,
    title: 'Smaller than 25mm'
  },
  {
    id: 100,
    title: 'Smaller than 10cm'
  },
  {
    id: 101,
    title: 'Larger than 10cm'
  },
  {
    id: 500,
    title: 'Smaller than 50cm'
  },
  {
    id: 501,
    title: 'Larger than 50cm'
  }
]
