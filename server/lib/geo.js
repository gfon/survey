import geoip from 'fast-geoip';
import logger from './logger.js';

const log = logger.getLogger('geoip');

const maskIps = ['127.0.0.1'];

export default (req, res, next) => {
  const clientIp =
    req.headers['x-real-ip'] ||
    (req.headers['x-forwarded-for'] &&
      req.headers['x-forwarded-for'].replace(/,.*$/, '')) ||
    req.ip ||
    null;

  if (clientIp && maskIps.indexOf(clientIp) === -1) {
    log.info('Requesting geo for', clientIp);
    geoip.lookup(clientIp).then((geo) => {
      if (res.json) {
        res.json(geo);
      } else {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(geo));
      }
    });
  } else {
    if (res.json) {
      res.json({});
    } else {
      res.setHeader('Content-Type', 'application/json');
      res.end('{}');
    }
  }
};
