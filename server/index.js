import express from 'express';
import logger from './lib/logger.js';
import { resolve } from 'path';
import geo from './lib/geo.js';

const log = logger.getLogger('serve');

const index = new URL('../dist/index.html', import.meta.url).pathname;

const app = express();

const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 7673;

process.on('SIGTERM', () => process.exit());

app.use((req, res, next) => {
  const start = Date.now();
  next();
  const ms = Date.now() - start;
  log.info(`${req.method} ${req.url} - ${res.statusCode} ${ms}ms`);
});

app.get('/geo', geo);
app.use(express.static(resolve('dist')));

// Send index for all non-handled responses
app.get('*', (req, res) => {
  res.sendFile(index);
});

app.listen(port, host);
log.info(`Listening on ${host}:${port}`);
