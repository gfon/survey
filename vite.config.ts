import { defineConfig } from 'vite';
import preact from '@preact/preset-vite';
import geo from './server/lib/geo';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    preact(),
    {
      name: 'surveyDev',
      configureServer(server) {
        server.middlewares.use('/geo', geo);
      }
    }
  ]
});
