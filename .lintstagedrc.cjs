module.exports = {
  '{src/README.md,CHANGELOG.md,config.example.js,package.json}': [
    () => 'utils/mdFileInclude.cjs src/README.md README.md',
    'git add README.md'
  ],
  '*.{scss,md}': [
    'prettier --write'
  ],
  '*.{ts,tsx,json}': [
    'prettier --write',
    'eslint -c .eslintrc.commit.cjs --fix'
  ]
};
